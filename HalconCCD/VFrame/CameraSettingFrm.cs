﻿using HalconCCD.Camera.Original;

namespace HalconCCD.VFrame
{
    public partial class CameraSettingFrm : Form
    {
        private readonly MindVison cam02;

        public CameraSettingFrm()
        {
            InitializeComponent();
            cam02 = new MindVison(this.Handle);
        }

        private void Setting_tsm_Click(object sender, EventArgs e)
        {
            cam02.CameraSet();
        }

        private void Start_tsm_Click(object sender, EventArgs e)
        {
            cam02.CyclicAcquisition(cameraImage_pic.Handle);
        }

        private void Stop_tsm_Click(object sender, EventArgs e)
        {

        }

        private void Snap_tsm_Click(object sender, EventArgs e)
        {
            cam02.SaveImage();
        }

        private void CameraSettingFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            cam02.Disconnect();
        }
    }
}
