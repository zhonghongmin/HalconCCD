﻿namespace HalconCCD.VFrame
{
    partial class DrawROIFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            myHalconControl1 = new VControl.MyHalconControl();
            DrawROI_btn = new Button();
            UpdateROI_btn = new Button();
            DropTemplate_btn = new Button();
            finish_btn = new Button();
            tableLayoutPanel1 = new TableLayoutPanel();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // myHalconControl1
            // 
            tableLayoutPanel1.SetColumnSpan(myHalconControl1, 4);
            myHalconControl1.Dock = DockStyle.Fill;
            myHalconControl1.Location = new Point(1, 1);
            myHalconControl1.Margin = new Padding(1);
            myHalconControl1.Name = "myHalconControl1";
            myHalconControl1.Size = new Size(702, 459);
            myHalconControl1.TabIndex = 0;
            // 
            // DrawROI_btn
            // 
            DrawROI_btn.BackColor = Color.Gray;
            DrawROI_btn.Dock = DockStyle.Fill;
            DrawROI_btn.Location = new Point(2, 463);
            DrawROI_btn.Margin = new Padding(2);
            DrawROI_btn.Name = "DrawROI_btn";
            DrawROI_btn.Size = new Size(172, 36);
            DrawROI_btn.TabIndex = 1;
            DrawROI_btn.Text = "选取模板图像";
            DrawROI_btn.UseVisualStyleBackColor = false;
            DrawROI_btn.Click += DrawROI_btn_Click;
            // 
            // UpdateROI_btn
            // 
            UpdateROI_btn.BackColor = Color.Gray;
            UpdateROI_btn.Dock = DockStyle.Fill;
            UpdateROI_btn.Location = new Point(178, 463);
            UpdateROI_btn.Margin = new Padding(2);
            UpdateROI_btn.Name = "UpdateROI_btn";
            UpdateROI_btn.Size = new Size(172, 36);
            UpdateROI_btn.TabIndex = 1;
            UpdateROI_btn.Text = "修改模板图像";
            UpdateROI_btn.UseVisualStyleBackColor = false;
            UpdateROI_btn.Click += UpdateROI_btn_Click;
            // 
            // DropTemplate_btn
            // 
            DropTemplate_btn.BackColor = Color.Gray;
            DropTemplate_btn.Dock = DockStyle.Fill;
            DropTemplate_btn.Location = new Point(354, 463);
            DropTemplate_btn.Margin = new Padding(2);
            DropTemplate_btn.Name = "DropTemplate_btn";
            DropTemplate_btn.Size = new Size(172, 36);
            DropTemplate_btn.TabIndex = 1;
            DropTemplate_btn.Text = "截取模板图像";
            DropTemplate_btn.UseVisualStyleBackColor = false;
            DropTemplate_btn.Click += DropTemplate_btn_Click;
            // 
            // finish_btn
            // 
            finish_btn.BackColor = Color.Gray;
            finish_btn.Dock = DockStyle.Fill;
            finish_btn.Location = new Point(530, 463);
            finish_btn.Margin = new Padding(2);
            finish_btn.Name = "finish_btn";
            finish_btn.Size = new Size(172, 36);
            finish_btn.TabIndex = 2;
            finish_btn.Text = "完成";
            finish_btn.UseVisualStyleBackColor = false;
            finish_btn.Click += Finish_btn_Click;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 4;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.Controls.Add(DrawROI_btn, 0, 1);
            tableLayoutPanel1.Controls.Add(myHalconControl1, 0, 0);
            tableLayoutPanel1.Controls.Add(finish_btn, 3, 1);
            tableLayoutPanel1.Controls.Add(UpdateROI_btn, 1, 1);
            tableLayoutPanel1.Controls.Add(DropTemplate_btn, 2, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel1.Size = new Size(704, 501);
            tableLayoutPanel1.TabIndex = 3;
            // 
            // DrawROIFrm
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(704, 501);
            Controls.Add(tableLayoutPanel1);
            Margin = new Padding(2);
            Name = "DrawROIFrm";
            StartPosition = FormStartPosition.CenterParent;
            Text = "截取模板图像窗口";
            Load += DrawROIFrm_Load;
            tableLayoutPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private VControl.MyHalconControl myHalconControl1;
        private Button DrawROI_btn;
        private Button UpdateROI_btn;
        private Button DropTemplate_btn;
        private Button finish_btn;
        private TableLayoutPanel tableLayoutPanel1;
    }
}