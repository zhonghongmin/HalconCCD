﻿namespace HalconCCD.VFrame
{
    partial class CameraSettingFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel1 = new TableLayoutPanel();
            cameraImage_pic = new PictureBox();
            menuStrip1 = new MenuStrip();
            setting_tsm = new ToolStripMenuItem();
            start_tsm = new ToolStripMenuItem();
            stop_tsm = new ToolStripMenuItem();
            snap_tsm = new ToolStripMenuItem();
            tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)cameraImage_pic).BeginInit();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(cameraImage_pic, 0, 1);
            tableLayoutPanel1.Controls.Add(menuStrip1, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Size = new Size(506, 354);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // cameraImage_pic
            // 
            cameraImage_pic.BackColor = Color.Black;
            cameraImage_pic.Dock = DockStyle.Fill;
            cameraImage_pic.Location = new Point(3, 33);
            cameraImage_pic.Name = "cameraImage_pic";
            cameraImage_pic.Size = new Size(500, 318);
            cameraImage_pic.TabIndex = 0;
            cameraImage_pic.TabStop = false;
            // 
            // menuStrip1
            // 
            menuStrip1.Dock = DockStyle.Fill;
            menuStrip1.Items.AddRange(new ToolStripItem[] { setting_tsm, start_tsm, stop_tsm, snap_tsm });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(506, 30);
            menuStrip1.TabIndex = 1;
            menuStrip1.Text = "menuStrip1";
            // 
            // setting_tsm
            // 
            setting_tsm.BackColor = Color.Transparent;
            setting_tsm.Name = "setting_tsm";
            setting_tsm.Size = new Size(68, 26);
            setting_tsm.Text = "相机设置";
            setting_tsm.Click += Setting_tsm_Click;
            // 
            // start_tsm
            // 
            start_tsm.Name = "start_tsm";
            start_tsm.Size = new Size(44, 26);
            start_tsm.Text = "播放";
            start_tsm.Click += Start_tsm_Click;
            // 
            // stop_tsm
            // 
            stop_tsm.Name = "stop_tsm";
            stop_tsm.Size = new Size(44, 26);
            stop_tsm.Text = "停止";
            stop_tsm.Click += Stop_tsm_Click;
            // 
            // snap_tsm
            // 
            snap_tsm.Name = "snap_tsm";
            snap_tsm.Size = new Size(44, 26);
            snap_tsm.Text = "抓图";
            snap_tsm.Click += Snap_tsm_Click;
            // 
            // CameraSettingFrm
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(506, 354);
            Controls.Add(tableLayoutPanel1);
            MainMenuStrip = menuStrip1;
            Name = "CameraSettingFrm";
            Text = "CameraSettingFrm";
            FormClosing += CameraSettingFrm_FormClosing;
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)cameraImage_pic).EndInit();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private PictureBox cameraImage_pic;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem setting_tsm;
        private ToolStripMenuItem start_tsm;
        private ToolStripMenuItem stop_tsm;
        private ToolStripMenuItem snap_tsm;
    }
}