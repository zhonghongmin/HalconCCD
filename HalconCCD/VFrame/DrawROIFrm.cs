﻿using HalconCCD.VControl;
using HalconDotNet;

namespace HalconCCD.VFrame
{
    public partial class DrawROIFrm : Form
    {
        private FlowNode _currentNode;
        private FlowNode _lastNode;
        private double row1;
        private double column1;
        private double row2;
        private double column2;
        private HWindow hv_Handle;

        public DrawROIFrm(FlowNode preNode, FlowNode currentNode)
        {
            InitializeComponent();
            this._currentNode = currentNode;
            this._lastNode = preNode;
        }

        private void DrawROIFrm_Load(object sender, EventArgs e)
        {
            hv_Handle = myHalconControl1.HalconWindow;
            hv_Handle.SetColor("red");
            hv_Handle.SetDraw("margin");
            // 获取输出的图像
           // myHalconControl1.DisplayImage(_lastNode.Output);
        }

        private void DrawROI_btn_Click(object sender, EventArgs e)
        {
            myHalconControl1.Focus();
            hv_Handle.DrawRectangle1(out row1, out column1, out row2, out column2);
            hv_Handle.DispRectangle1(row1, column1, row2, column2);

        }

        private void UpdateROI_btn_Click(object sender, EventArgs e)
        {
            hv_Handle.ClearWindow();
            
            myHalconControl1.DisplayImage(_lastNode.Output);
            
            myHalconControl1.Focus();
            hv_Handle.DrawRectangle1Mod(row1, column1, row2, column2, out row1, out column1, out row2, out column2);
            hv_Handle.DispRectangle1(row1, column1, row2, column2);
        }

        private void DropTemplate_btn_Click(object sender, EventArgs e)
        {
            hv_Handle.ClearWindow();
            
            HOperatorSet.GenRectangle1(out HObject rect, row1, column1, row2, column2);
            
            HOperatorSet.ReduceDomain(_lastNode.Output, rect, out HObject ImageReduced);
            HOperatorSet.CropDomain(ImageReduced, out HObject ImagePart);
            
            myHalconControl1.DisplayImage(ImagePart);
            
            _currentNode.Input = _lastNode.Output;
            _currentNode.Output = ImagePart;
            _currentNode.RoiOutput = new HTuple[] { row1, column1, row2, column2 };
        }

        private void Finish_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
