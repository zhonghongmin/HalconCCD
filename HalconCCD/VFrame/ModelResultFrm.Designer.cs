﻿namespace HalconCCD.VFrame
{
    partial class modelResultFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            hw = new VControl.MyHalconControl();
            LoadPicture_btn = new Button();
            GetMatchResult_btn = new Button();
            SelectTemplate_btn = new Button();
            TakePicture_btn = new Button();
            tableLayoutPanel1 = new TableLayoutPanel();
            groupBox1 = new GroupBox();
            comboBox1 = new ComboBox();
            label5 = new Label();
            label4 = new Label();
            label8 = new Label();
            label2 = new Label();
            label7 = new Label();
            label6 = new Label();
            label3 = new Label();
            numericUpDown7 = new NumericUpDown();
            label1 = new Label();
            numericUpDown6 = new NumericUpDown();
            numericUpDown3 = new NumericUpDown();
            numericUpDown2 = new NumericUpDown();
            numericUpDown5 = new NumericUpDown();
            numericUpDown4 = new NumericUpDown();
            numericUpDown1 = new NumericUpDown();
            numericUpDown8 = new NumericUpDown();
            label9 = new Label();
            tableLayoutPanel1.SuspendLayout();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown8).BeginInit();
            SuspendLayout();
            // 
            // hw
            // 
            hw.Dock = DockStyle.Fill;
            hw.Location = new Point(1, 1);
            hw.Margin = new Padding(1);
            hw.Name = "hw";
            tableLayoutPanel1.SetRowSpan(hw, 4);
            hw.Size = new Size(288, 174);
            hw.TabIndex = 0;
            // 
            // LoadPicture_btn
            // 
            LoadPicture_btn.BackColor = Color.Turquoise;
            LoadPicture_btn.Dock = DockStyle.Fill;
            LoadPicture_btn.Location = new Point(292, 46);
            LoadPicture_btn.Margin = new Padding(2);
            LoadPicture_btn.Name = "LoadPicture_btn";
            LoadPicture_btn.Size = new Size(190, 40);
            LoadPicture_btn.TabIndex = 1;
            LoadPicture_btn.Text = "加载本地图片";
            LoadPicture_btn.UseVisualStyleBackColor = false;
            LoadPicture_btn.Click += LoadPicture_btn_Click;
            // 
            // GetMatchResult_btn
            // 
            GetMatchResult_btn.BackColor = Color.Turquoise;
            GetMatchResult_btn.Dock = DockStyle.Fill;
            GetMatchResult_btn.Location = new Point(292, 134);
            GetMatchResult_btn.Margin = new Padding(2);
            GetMatchResult_btn.Name = "GetMatchResult_btn";
            GetMatchResult_btn.Size = new Size(190, 40);
            GetMatchResult_btn.TabIndex = 1;
            GetMatchResult_btn.Text = "测试结果";
            GetMatchResult_btn.UseVisualStyleBackColor = false;
            GetMatchResult_btn.Click += GetMatchResult_btn_Click;
            // 
            // SelectTemplate_btn
            // 
            SelectTemplate_btn.BackColor = Color.Turquoise;
            SelectTemplate_btn.Dock = DockStyle.Fill;
            SelectTemplate_btn.Location = new Point(292, 2);
            SelectTemplate_btn.Margin = new Padding(2);
            SelectTemplate_btn.Name = "SelectTemplate_btn";
            SelectTemplate_btn.Size = new Size(190, 40);
            SelectTemplate_btn.TabIndex = 1;
            SelectTemplate_btn.Text = "选择模板及轮廓";
            SelectTemplate_btn.UseVisualStyleBackColor = false;
            SelectTemplate_btn.Click += SelectTemplate_btn_Click;
            // 
            // TakePicture_btn
            // 
            TakePicture_btn.BackColor = Color.Turquoise;
            TakePicture_btn.Dock = DockStyle.Fill;
            TakePicture_btn.Location = new Point(292, 90);
            TakePicture_btn.Margin = new Padding(2);
            TakePicture_btn.Name = "TakePicture_btn";
            TakePicture_btn.Size = new Size(190, 40);
            TakePicture_btn.TabIndex = 1;
            TakePicture_btn.Text = "拍照";
            TakePicture_btn.UseVisualStyleBackColor = false;
            TakePicture_btn.Click += TakePicture_btn_Click;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 60F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40F));
            tableLayoutPanel1.Controls.Add(hw, 0, 0);
            tableLayoutPanel1.Controls.Add(GetMatchResult_btn, 1, 3);
            tableLayoutPanel1.Controls.Add(SelectTemplate_btn, 1, 0);
            tableLayoutPanel1.Controls.Add(TakePicture_btn, 1, 2);
            tableLayoutPanel1.Controls.Add(LoadPicture_btn, 1, 1);
            tableLayoutPanel1.Controls.Add(groupBox1, 0, 4);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 6;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 12.3711357F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 12.3711338F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 12.3711338F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 12.3711338F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 25.2577324F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 25.2577324F));
            tableLayoutPanel1.Size = new Size(484, 361);
            tableLayoutPanel1.TabIndex = 2;
            // 
            // groupBox1
            // 
            groupBox1.BackColor = Color.Transparent;
            tableLayoutPanel1.SetColumnSpan(groupBox1, 2);
            groupBox1.Controls.Add(comboBox1);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(label8);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label9);
            groupBox1.Controls.Add(label7);
            groupBox1.Controls.Add(label6);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(numericUpDown8);
            groupBox1.Controls.Add(numericUpDown7);
            groupBox1.Controls.Add(label1);
            groupBox1.Controls.Add(numericUpDown6);
            groupBox1.Controls.Add(numericUpDown3);
            groupBox1.Controls.Add(numericUpDown2);
            groupBox1.Controls.Add(numericUpDown5);
            groupBox1.Controls.Add(numericUpDown4);
            groupBox1.Controls.Add(numericUpDown1);
            groupBox1.Dock = DockStyle.Fill;
            groupBox1.Location = new Point(3, 179);
            groupBox1.Name = "groupBox1";
            tableLayoutPanel1.SetRowSpan(groupBox1, 2);
            groupBox1.Size = new Size(478, 179);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            groupBox1.Text = "用户参数";
            // 
            // comboBox1
            // 
            comboBox1.FormattingEnabled = true;
            comboBox1.Items.AddRange(new object[] { "none", "interpolation", "least_squares", "least_squares_high", "least_squares_very_high" });
            comboBox1.Location = new Point(92, 134);
            comboBox1.Name = "comboBox1";
            comboBox1.Size = new Size(123, 25);
            comboBox1.TabIndex = 2;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(30, 137);
            label5.Name = "label5";
            label5.Size = new Size(56, 17);
            label5.TabIndex = 1;
            label5.Text = "亚像素：";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(18, 111);
            label4.Name = "label4";
            label4.Size = new Size(68, 17);
            label4.TabIndex = 1;
            label4.Text = "最大重叠：";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(182, 54);
            label8.Name = "label8";
            label8.Size = new Size(104, 17);
            label8.TabIndex = 1;
            label8.Text = "最大金字塔级别：";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(6, 54);
            label2.Name = "label2";
            label2.Size = new Size(80, 17);
            label2.TabIndex = 1;
            label2.Text = "最大匹配数：";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(218, 82);
            label7.Name = "label7";
            label7.Size = new Size(68, 17);
            label7.TabIndex = 1;
            label7.Text = "起始角度：";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(218, 25);
            label6.Name = "label6";
            label6.Size = new Size(68, 17);
            label6.TabIndex = 1;
            label6.Text = "最大变形：";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(18, 82);
            label3.Name = "label3";
            label3.Size = new Size(68, 17);
            label3.TabIndex = 1;
            label3.Text = "贪心算法：";
            // 
            // numericUpDown7
            // 
            numericUpDown7.DecimalPlaces = 2;
            numericUpDown7.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            numericUpDown7.Location = new Point(292, 76);
            numericUpDown7.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown7.Name = "numericUpDown7";
            numericUpDown7.Size = new Size(67, 23);
            numericUpDown7.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(18, 25);
            label1.Name = "label1";
            label1.Size = new Size(68, 17);
            label1.TabIndex = 1;
            label1.Text = "匹配分数：";
            // 
            // numericUpDown6
            // 
            numericUpDown6.Location = new Point(292, 48);
            numericUpDown6.Name = "numericUpDown6";
            numericUpDown6.Size = new Size(67, 23);
            numericUpDown6.TabIndex = 0;
            // 
            // numericUpDown3
            // 
            numericUpDown3.DecimalPlaces = 2;
            numericUpDown3.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            numericUpDown3.Location = new Point(92, 76);
            numericUpDown3.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown3.Name = "numericUpDown3";
            numericUpDown3.Size = new Size(67, 23);
            numericUpDown3.TabIndex = 0;
            // 
            // numericUpDown2
            // 
            numericUpDown2.Location = new Point(92, 48);
            numericUpDown2.Name = "numericUpDown2";
            numericUpDown2.Size = new Size(67, 23);
            numericUpDown2.TabIndex = 0;
            // 
            // numericUpDown5
            // 
            numericUpDown5.DecimalPlaces = 2;
            numericUpDown5.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            numericUpDown5.Location = new Point(292, 19);
            numericUpDown5.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown5.Name = "numericUpDown5";
            numericUpDown5.Size = new Size(67, 23);
            numericUpDown5.TabIndex = 0;
            numericUpDown5.Value = new decimal(new int[] { 50, 0, 0, 131072 });
            // 
            // numericUpDown4
            // 
            numericUpDown4.DecimalPlaces = 2;
            numericUpDown4.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            numericUpDown4.Location = new Point(92, 105);
            numericUpDown4.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown4.Name = "numericUpDown4";
            numericUpDown4.Size = new Size(67, 23);
            numericUpDown4.TabIndex = 0;
            // 
            // numericUpDown1
            // 
            numericUpDown1.DecimalPlaces = 2;
            numericUpDown1.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            numericUpDown1.Location = new Point(92, 19);
            numericUpDown1.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown1.Name = "numericUpDown1";
            numericUpDown1.Size = new Size(67, 23);
            numericUpDown1.TabIndex = 0;
            numericUpDown1.Value = new decimal(new int[] { 50, 0, 0, 131072 });
            // 
            // numericUpDown8
            // 
            numericUpDown8.DecimalPlaces = 2;
            numericUpDown8.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            numericUpDown8.Location = new Point(292, 105);
            numericUpDown8.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown8.Name = "numericUpDown8";
            numericUpDown8.Size = new Size(67, 23);
            numericUpDown8.TabIndex = 0;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(218, 111);
            label9.Name = "label9";
            label9.Size = new Size(68, 17);
            label9.TabIndex = 1;
            label9.Text = "角度范围：";
            // 
            // modelResultFrm
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(484, 361);
            Controls.Add(tableLayoutPanel1);
            Margin = new Padding(2);
            Name = "modelResultFrm";
            StartPosition = FormStartPosition.CenterParent;
            Text = "模板匹配结果";
            tableLayoutPanel1.ResumeLayout(false);
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown7).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown6).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown3).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown2).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown5).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown4).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDown8).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private HalconCCD.VControl.MyHalconControl hw;
        private Button button1;
        private Button GetMatchResult_btn;
        private Button button4;
        private Button btn;
        private TableLayoutPanel tableLayoutPanel1;
        private GroupBox groupBox1;
        private Button SelectTemplate_btn;
        private Button TakePicture_btn;
        private Button LoadPicture_btn;
        private NumericUpDown numericUpDown2;
        private NumericUpDown numericUpDown1;
        private Label label2;
        private Label label1;
        private Label label4;
        private Label label3;
        private NumericUpDown numericUpDown3;
        private NumericUpDown numericUpDown4;
        private ComboBox comboBox1;
        private Label label5;
        private Label label8;
        private Label label7;
        private Label label6;
        private NumericUpDown numericUpDown7;
        private NumericUpDown numericUpDown6;
        private NumericUpDown numericUpDown5;
        private Label label9;
        private NumericUpDown numericUpDown8;
    }
}