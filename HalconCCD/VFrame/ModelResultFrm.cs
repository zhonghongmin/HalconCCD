﻿using HalconCCD.VControl;
using HalconDotNet;
using System.Data;
using MVSDK;
using CameraHandle = System.Int32;
using MvApi = MVSDK.MvApi;

namespace HalconCCD.VFrame

{
    public partial class modelResultFrm : Form
    {
        /// <summary>
        /// 上一个节点
        /// </summary>
        private FlowNode _lastNode;

        /// <summary>
        /// 当前节点
        /// </summary>
        private FlowNode _curentNode;

        /// <summary>
        /// 模板轮廓
        /// </summary>
        private HObject _modelOutline;

        /// <summary>
        /// 模板ID
        /// </summary>
        private HTuple _modelId;

        /// <summary>
        /// 需匹配的图像
        /// </summary>
        private HObject _image;

        public modelResultFrm(FlowNode lastNode, FlowNode currentNode)
        {
            InitializeComponent();
            this._lastNode = lastNode;
            this._curentNode = currentNode;
        }

        public modelResultFrm()
        {
            InitializeComponent();
        }

        private void SelectTemplate_btn_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Title = "加载模板及轮廓";
            dialog.Filter = "模板及轮廓|*.shm;*.hobj";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            dialog.Multiselect = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var files = dialog.FileNames;

                // 获取对应的模板ID
                var modelFile = files.Where(filename => filename.Contains(".shm")).FirstOrDefault();
                HOperatorSet.ReadShapeModel(modelFile, out _modelId);

                //获取模板轮廓
                HOperatorSet.GetShapeModelContours(out _modelOutline, _modelId, 1);//模板中提取
                // var outLineFile = files.Where(filename => filename.Contains(".hobj")).FirstOrDefault();
                // HOperatorSet.ReadObject(out _modelOutline, outLineFile);


            }
        }

        private void LoadPicture_btn_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Title = "待匹配图像",
                Filter = "匹配图像|*.png;*.jpg;*.bmp"
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var fileName = dialog.FileName;
                _image = new HImage(fileName);
                hw.DisplayImage(_image);
            }
            //_curentNode.Output = _image;
        }

        private void GetMatchResult_btn_Click(object sender, EventArgs e)
        {
            SetDisplayFont(hw.HalconWindow, 12, "mono", "true", "false");

            // 根据模板查找图像中的匹配结果
            ModelMath(_image);

        }

        private void ModelMath(HObject L_Image)
        {
            HTuple hv_Score = new HTuple();


            HOperatorSet.FindShapeModel(L_Image, _modelId, 0, (new HTuple(360)).TupleRad(), 0.2, 1, 0.5, "least_squares", 0, 0.7, out HTuple hv_Row1, out HTuple hv_Column1, out HTuple hv_Angle, out hv_Score);
            if (hv_Score.Type == HTupleType.EMPTY)
            {
                HOperatorSet.DispText(hw.HalconWindow, "配置失败", "window", 12, 12, "red", new HTuple(), new HTuple());
                return;
            }

            // 创建一个齐次矩阵
            HOperatorSet.HomMat2dIdentity(out HTuple hv_HomMat2DIdentity);
            //模板仿射变换
            HOperatorSet.HomMat2dTranslate(hv_HomMat2DIdentity, hv_Row1, hv_Column1, out HTuple hv_HomMat2DTranslate);
            // 旋转
            HOperatorSet.HomMat2dRotate(hv_HomMat2DTranslate, hv_Angle, hv_Row1, hv_Column1, out HTuple hv_HomMat2DRotate);
            // 仿射变换轮廓
            HOperatorSet.AffineTransContourXld(_modelOutline, out HObject ho_ContoursAffineTrans, hv_HomMat2DRotate);
            //HOperatorSet.AffineTransImage();

            // 显示结果
            HOperatorSet.SetColor(hw.HalconWindow, "green");
            HOperatorSet.SetLineWidth(hw.HalconWindow, 2);
            HOperatorSet.DispObj(L_Image, hw.HalconWindow);
            HOperatorSet.DispObj(ho_ContoursAffineTrans, hw.HalconWindow);

            // 获取轮廓中心
            HOperatorSet.AreaCenterXld(ho_ContoursAffineTrans, out HTuple Area, out HTuple xldRow, out HTuple xldColumn, out HTuple _);
            HOperatorSet.SetColor(hw.HalconWindow, "green");
            HOperatorSet.SetLineWidth(hw.HalconWindow, 1);
            // 绘制十字准星
            //HOperatorSet.DispCross(hw.HalconWindow, xldRow, xldColumn, 60, 0);


            HOperatorSet.DispText(hw.HalconWindow, $"row={hv_Row1.TupleString(".2f")}\r\ncolumn={hv_Column1:f2}\r\nangle={hv_Angle}", "image", 20, 20, "black", new HTuple(), new HTuple());
           
        }


        public void SetDisplayFont(HTuple hv_WindowHandle, HTuple hv_Size, HTuple hv_Font, HTuple hv_Bold, HTuple hv_Slant)
        {

            HTuple hv_OS = new HTuple(), hv_Fonts = new HTuple();
            HTuple hv_Style = new HTuple(), hv_Exception = new HTuple();
            HTuple hv_AvailableFonts = new HTuple(), hv_Fdx = new HTuple();
            HTuple hv_Indices = new HTuple();
            HTuple hv_Font_COPY_INP_TMP = new HTuple(hv_Font);
            HTuple hv_Size_COPY_INP_TMP = new HTuple(hv_Size);


            try
            {

                HOperatorSet.GetSystem("operating_system", out hv_OS);
                if ((int)((new HTuple(hv_Size_COPY_INP_TMP.TupleEqual(new HTuple()))).TupleOr(
                    new HTuple(hv_Size_COPY_INP_TMP.TupleEqual(-1)))) != 0)
                {

                    hv_Size_COPY_INP_TMP = 16;
                }
                if ((int)(new HTuple(((hv_OS.TupleSubstr(0, 2))).TupleEqual("Win"))) != 0)
                {
                    using (HDevDisposeHelper dh = new HDevDisposeHelper())
                    {
                        {
                            HTuple ExpTmpLocalVar_Size = ((1.13677 * hv_Size_COPY_INP_TMP)).TupleInt();

                            hv_Size_COPY_INP_TMP = ExpTmpLocalVar_Size;
                        }
                    }
                }
                else
                {
                    using (HDevDisposeHelper dh = new HDevDisposeHelper())
                    {
                        {
                            HTuple ExpTmpLocalVar_Size = hv_Size_COPY_INP_TMP.TupleInt();

                            hv_Size_COPY_INP_TMP = ExpTmpLocalVar_Size;
                        }
                    }
                }
                if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("Courier"))) != 0)
                {

                    hv_Fonts = new HTuple();
                    hv_Fonts[0] = "Courier";
                    hv_Fonts[1] = "Courier 10 Pitch";
                    hv_Fonts[2] = "Courier New";
                    hv_Fonts[3] = "CourierNew";
                    hv_Fonts[4] = "Liberation Mono";
                }
                else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("mono"))) != 0)
                {

                    hv_Fonts = new HTuple();
                    hv_Fonts[0] = "Consolas";
                    hv_Fonts[1] = "Menlo";
                    hv_Fonts[2] = "Courier";
                    hv_Fonts[3] = "Courier 10 Pitch";
                    hv_Fonts[4] = "FreeMono";
                    hv_Fonts[5] = "Liberation Mono";
                }
                else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("sans"))) != 0)
                {

                    hv_Fonts = new HTuple();
                    hv_Fonts[0] = "Luxi Sans";
                    hv_Fonts[1] = "DejaVu Sans";
                    hv_Fonts[2] = "FreeSans";
                    hv_Fonts[3] = "Arial";
                    hv_Fonts[4] = "Liberation Sans";
                }
                else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("serif"))) != 0)
                {

                    hv_Fonts = new HTuple();
                    hv_Fonts[0] = "Times New Roman";
                    hv_Fonts[1] = "Luxi Serif";
                    hv_Fonts[2] = "DejaVu Serif";
                    hv_Fonts[3] = "FreeSerif";
                    hv_Fonts[4] = "Utopia";
                    hv_Fonts[5] = "Liberation Serif";
                }
                else
                {

                    hv_Fonts = new HTuple(hv_Font_COPY_INP_TMP);
                }

                hv_Style = "";
                if ((int)(new HTuple(hv_Bold.TupleEqual("true"))) != 0)
                {
                    using (HDevDisposeHelper dh = new HDevDisposeHelper())
                    {
                        {
                            HTuple ExpTmpLocalVar_Style = hv_Style + "Bold";

                            hv_Style = ExpTmpLocalVar_Style;
                        }
                    }
                }
                else if ((int)(new HTuple(hv_Bold.TupleNotEqual("false"))) != 0)
                {

                    hv_Exception = "Wrong value of control parameter Bold";
                    throw new HalconException(hv_Exception);
                }
                if ((int)(new HTuple(hv_Slant.TupleEqual("true"))) != 0)
                {
                    using (HDevDisposeHelper dh = new HDevDisposeHelper())
                    {
                        {
                            HTuple ExpTmpLocalVar_Style = hv_Style + "Italic";
                            hv_Style = ExpTmpLocalVar_Style;
                        }
                    }
                }
                else if ((int)(new HTuple(hv_Slant.TupleNotEqual("false"))) != 0)
                {

                    hv_Exception = "Wrong value of control parameter Slant";
                    throw new HalconException(hv_Exception);
                }
                if ((int)(new HTuple(hv_Style.TupleEqual(""))) != 0)
                {

                    hv_Style = "Normal";
                }

                HOperatorSet.QueryFont(hv_WindowHandle, out hv_AvailableFonts);

                hv_Font_COPY_INP_TMP = "";
                for (hv_Fdx = 0; (int)hv_Fdx <= (int)((new HTuple(hv_Fonts.TupleLength())) - 1); hv_Fdx = (int)hv_Fdx + 1)
                {

                    using (HDevDisposeHelper dh = new HDevDisposeHelper())
                    {
                        hv_Indices = hv_AvailableFonts.TupleFind(hv_Fonts.TupleSelect(hv_Fdx));
                    }
                    if ((int)(new HTuple((new HTuple(hv_Indices.TupleLength())).TupleGreater(0))) != 0)
                    {
                        if ((int)(new HTuple(((hv_Indices.TupleSelect(0))).TupleGreaterEqual(0))) != 0)
                        {

                            using (HDevDisposeHelper dh = new HDevDisposeHelper())
                            {
                                hv_Font_COPY_INP_TMP = hv_Fonts.TupleSelect(hv_Fdx);
                            }
                            break;
                        }
                    }
                }
                if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual(""))) != 0)
                {
                    throw new HalconException("Wrong value of control parameter Font");
                }
                using (HDevDisposeHelper dh = new HDevDisposeHelper())
                {
                    {
                        HTuple ExpTmpLocalVar_Font = (((hv_Font_COPY_INP_TMP + "-") + hv_Style) + "-") + hv_Size_COPY_INP_TMP;

                        hv_Font_COPY_INP_TMP = ExpTmpLocalVar_Font;
                    }
                }
                HOperatorSet.SetFont(hv_WindowHandle, hv_Font_COPY_INP_TMP);

                return;
            }
            catch (HalconException HDevExpDefaultException)
            {

                throw HDevExpDefaultException;
            }
        }

        private void TakePicture_btn_Click(object sender, EventArgs e)
        {
            Action();
            //if (m_Grabber != IntPtr.Zero)
            //    MvApi.CameraGrabber_SaveImageAsync(m_Grabber);
        }


        protected IntPtr m_Grabber = IntPtr.Zero;
        protected CameraHandle m_hCamera = 0;
        protected tSdkCameraDevInfo m_DevInfo;
        protected pfnCameraGrabberSaveImageComplete m_SaveImageComplete;
        private void InitC()
        {
            m_SaveImageComplete = new pfnCameraGrabberSaveImageComplete(CameraGrabberSaveImageComplete);

            if (MvApi.CameraGrabber_CreateFromDevicePage(out m_Grabber) == CameraSdkStatus.CAMERA_STATUS_SUCCESS)
            {
                MvApi.CameraGrabber_GetCameraDevInfo(m_Grabber, out m_DevInfo);
                MvApi.CameraGrabber_GetCameraHandle(m_Grabber, out m_hCamera);
                MvApi.CameraCreateSettingPage(m_hCamera, this.Handle, m_DevInfo.acFriendlyName, null, (IntPtr)0, 0);

                MvApi.CameraGrabber_SetSaveImageCompleteCallback(m_Grabber, m_SaveImageComplete, IntPtr.Zero);

                // 黑白相机设置ISP输出灰度图像
                // 彩色相机ISP默认会输出BGR24图像
                tSdkCameraCapbility cap;
                MvApi.CameraGetCapability(m_hCamera, out cap);
                if (cap.sIspCapacity.bMonoSensor != 0)
                {
                    MvApi.CameraSetIspOutFormat(m_hCamera, (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_MONO8);
                }

                // MvApi.CameraGrabber_SetHWnd(m_Grabber,"");
                MvApi.CameraGrabber_StartLive(m_Grabber);
            }
        }
        private void CameraGrabberSaveImageComplete(
           IntPtr Grabber,
           IntPtr Image,    // 需要调用CameraImage_Destroy释放
           CameraSdkStatus Status,
           IntPtr Context)
        {
            if (Image != IntPtr.Zero)
            {
                string filename = Path.Combine(
                        AppDomain.CurrentDomain.BaseDirectory.ToString(),
                        $"{DateTime.Now:G}");

                MvApi.CameraImage_SaveAsBmp(Image, filename);

                MessageBox.Show(filename);
            }

            MvApi.CameraImage_Destroy(Image);
        }

        public void Action()
        {


            // Local iconic variables 

            HObject ho_ModelContours, ho_TransContours;
            HObject ho_Image = null, ho_RectifiedImage = null;

            // Local control variables 

            HTuple hv_AcqHandle = new HTuple(), hv_ModelID = new HTuple();
            HTuple hv_Row1 = new HTuple(), hv_Column1 = new HTuple();
            HTuple hv_Row2 = new HTuple(), hv_Column2 = new HTuple();
            HTuple hv_RefRow = new HTuple(), hv_RefColumn = new HTuple();
            HTuple hv_HomMat2D = new HTuple(), hv_Row = new HTuple();
            HTuple hv_Column = new HTuple(), hv_Angle = new HTuple();
            HTuple hv_Score = new HTuple(), hv_I = new HTuple(), hv_AlignmentHomMat2D = new HTuple();
            HTuple hv_RectificationHomMat2D = new HTuple();
            // Initialize local and output iconic variables 
            HOperatorSet.GenEmptyObj(out ho_ModelContours);
            HOperatorSet.GenEmptyObj(out ho_TransContours);
            HOperatorSet.GenEmptyObj(out ho_Image);
            HOperatorSet.GenEmptyObj(out ho_RectifiedImage);
            //
            //Matching 01: ************************************************
            //Matching 01: BEGIN of generated code for model initialization
            //Matching 01: ************************************************
            HOperatorSet.SetSystem("border_shape_models", "false");
            //
            //Matching 01: Initialize acquisition
            hv_AcqHandle.Dispose();
            HOperatorSet.OpenFramegrabber("GigEVision2", 0, 0, 0, 0, 0, 0, "progressive",
                -1, "default", -1, "false", "default", "CAM02", 0, -1, out hv_AcqHandle);
            //
            //Matching 01: Read the shape model from file
            hv_ModelID.Dispose();
            HOperatorSet.ReadShapeModel("C:/Users/zyl/Documents/MySoftware/HalconCCD/HalconCCD/bin/Debug/net6.0-windows/Template/Matching1215.shm",
                out hv_ModelID);
            //
            //Matching 01: Get the model contour for transforming it later into the image
            ho_ModelContours.Dispose();
            HOperatorSet.GetShapeModelContours(out ho_ModelContours, hv_ModelID, 1);
            //
            //Matching 01: Set a reference position to show the model
            hv_Row1.Dispose(); hv_Column1.Dispose(); hv_Row2.Dispose(); hv_Column2.Dispose();
            HOperatorSet.SmallestRectangle1Xld(ho_ModelContours, out hv_Row1, out hv_Column1,
                out hv_Row2, out hv_Column2);
            hv_RefRow.Dispose();
            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            {
                hv_RefRow = ((hv_Row2.TupleMax()
                    ) - (hv_Row1.TupleMin())) / 2;
            }
            hv_RefColumn.Dispose();
            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            {
                hv_RefColumn = ((hv_Column2.TupleMax()
                    ) - (hv_Column1.TupleMin())) / 2;
            }
            hv_HomMat2D.Dispose();
            HOperatorSet.VectorAngleToRigid(0, 0, 0, hv_RefRow, hv_RefColumn, 0, out hv_HomMat2D);
            ho_TransContours.Dispose();
            HOperatorSet.AffineTransContourXld(ho_ModelContours, out ho_TransContours, hv_HomMat2D);
            //
            //Matching 01: Display the model contours
            // if (HDevWindowStack.IsOpen())
            //{
            HOperatorSet.SetColor(hw.HalconWindow, "green");
            // }
            // if (HDevWindowStack.IsOpen())
            // {
            HOperatorSet.SetDraw(hw.HalconWindow, "margin");
            // }
            // if (HDevWindowStack.IsOpen())
            // {
            HOperatorSet.DispObj(ho_TransContours, hw.HalconWindow);
            // }
            // stop(...); only in hdevelop
            //
            //Matching 01: END of generated code for model initialization
            //Matching 01:  * * * * * * * * * * * * * * * * * * * * * * *
            //Matching 01: BEGIN of generated code for model application
            //
            //Task.Run(() =>
            //{

            //    while ((int)(1) != 0)
            //    {
            //        //
            //        //Matching 01: Obtain the image
            //        // ho_Image.Dispose();
            //        HOperatorSet.GrabImage(out ho_Image, hv_AcqHandle);
            //        //
            //        //Matching 01: Find the model
            //        using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //        {
            //            hv_Row.Dispose(); hv_Column.Dispose(); hv_Angle.Dispose(); hv_Score.Dispose();
            //            HOperatorSet.FindShapeModel(ho_Image, hv_ModelID, (new HTuple(0)).TupleRad()
            //                , (new HTuple(360)).TupleRad(), 0.89, 0, 0.5, "least_squares", (new HTuple(2)).TupleConcat(
            //                1), 0.75, out hv_Row, out hv_Column, out hv_Angle, out hv_Score);
            //        }
            //        //
            //        //Matching 01: Transform the model contours into the detected positions
            //        //将模型轮廓转换为检测位置

            //        HOperatorSet.DispObj(ho_Image, hw.HalconWindow);

            //        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
            //        {
            //            hv_HomMat2D.Dispose();
            //            HOperatorSet.HomMat2dIdentity(out hv_HomMat2D);
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dRotate(hv_HomMat2D, hv_Angle.TupleSelect(hv_I), 0, 0,
            //                    out ExpTmpOutVar_0);
            //                hv_HomMat2D.Dispose();
            //                hv_HomMat2D = ExpTmpOutVar_0;
            //            }
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dTranslate(hv_HomMat2D, hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(
            //                    hv_I), out ExpTmpOutVar_0);
            //                hv_HomMat2D.Dispose();
            //                hv_HomMat2D = ExpTmpOutVar_0;
            //            }
            //            ho_TransContours.Dispose();
            //            HOperatorSet.AffineTransContourXld(ho_ModelContours, out ho_TransContours,
            //                hv_HomMat2D);

            //            HOperatorSet.SetColor(hw.HalconWindow, "green");

            //            HOperatorSet.DispObj(ho_TransContours, hw.HalconWindow);

            //            // stop(...); only in hdevelop
            //        }
            //        //
            //        //Matching 01: Code for alignment of e.g. measurements
            //        //Matching 01: Calculate a hom_mat2d for each of the matching results
            //        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
            //        {
            //            hv_AlignmentHomMat2D.Dispose();
            //            HOperatorSet.HomMat2dIdentity(out hv_AlignmentHomMat2D);
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dTranslate(hv_AlignmentHomMat2D, -hv_RefRow, -hv_RefColumn,
            //                    out ExpTmpOutVar_0);
            //                hv_AlignmentHomMat2D.Dispose();
            //                hv_AlignmentHomMat2D = ExpTmpOutVar_0;
            //            }
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dRotate(hv_AlignmentHomMat2D, hv_Angle.TupleSelect(hv_I),
            //                    0, 0, out ExpTmpOutVar_0);
            //                hv_AlignmentHomMat2D.Dispose();
            //                hv_AlignmentHomMat2D = ExpTmpOutVar_0;
            //            }
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dTranslate(hv_AlignmentHomMat2D, hv_Row.TupleSelect(hv_I),
            //                    hv_Column.TupleSelect(hv_I), out ExpTmpOutVar_0);
            //                hv_AlignmentHomMat2D.Dispose();
            //                hv_AlignmentHomMat2D = ExpTmpOutVar_0;
            //            }
            //            //Matching 01: Insert your code using the alignment here, e.g. code generated by
            //            //Matching 01: the measure assistant with the code generation option
            //            //Matching 01: 'Alignment Method' set to 'Affine Transformation'.
            //        }
            //        //
            //        //Matching 01: Code for rectification of the image
            //        //Matching 01: Calculate an inverse hom_mat2d for each of the matching results
            //        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
            //        {
            //            hv_RectificationHomMat2D.Dispose();
            //            HOperatorSet.HomMat2dIdentity(out hv_RectificationHomMat2D);
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dTranslate(hv_RectificationHomMat2D, hv_RefRow - (hv_Row.TupleSelect(
            //                    hv_I)), hv_RefColumn - (hv_Column.TupleSelect(hv_I)), out ExpTmpOutVar_0);
            //                hv_RectificationHomMat2D.Dispose();
            //                hv_RectificationHomMat2D = ExpTmpOutVar_0;
            //            }
            //            using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //            {
            //                HTuple ExpTmpOutVar_0;
            //                HOperatorSet.HomMat2dRotate(hv_RectificationHomMat2D, -(hv_Angle.TupleSelect(
            //                    hv_I)), hv_RefRow, hv_RefColumn, out ExpTmpOutVar_0);
            //                hv_RectificationHomMat2D.Dispose();
            //                hv_RectificationHomMat2D = ExpTmpOutVar_0;
            //            }
            //            ho_RectifiedImage.Dispose();
            //            HOperatorSet.AffineTransImage(ho_Image, out ho_RectifiedImage, hv_RectificationHomMat2D,
            //                "constant", "false");
            //            //
            //            //Matching 01: Insert your code using the rectified image here
            //        }
            //    }

            //});
            //
            //Matching 01: *******************************************
            //Matching 01: END of generated code for model application
            //Matching 01: *******************************************
            //
            //ho_ModelContours.Dispose();
            //ho_TransContours.Dispose();
            //ho_Image.Dispose();
            //ho_RectifiedImage.Dispose();

            ////hv_AcqHandle.Dispose();
            //hv_ModelID.Dispose();
            //hv_Row1.Dispose();
            //hv_Column1.Dispose();
            //hv_Row2.Dispose();
            //hv_Column2.Dispose();
            //hv_RefRow.Dispose();
            //hv_RefColumn.Dispose();
            //hv_HomMat2D.Dispose();
            //hv_Row.Dispose();
            //hv_Column.Dispose();
            //hv_Angle.Dispose();
            //hv_Score.Dispose();
            //hv_I.Dispose();
            //hv_AlignmentHomMat2D.Dispose();
            //hv_RectificationHomMat2D.Dispose();

        }
    }
}
