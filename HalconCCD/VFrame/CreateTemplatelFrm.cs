﻿using HalconCCD.VControl;
using HalconDotNet;

namespace HalconCCD.VFrame
{
    public partial class CreateTemplatelFrm : Form
    {

        private FlowNode _lastNode;

        private FlowNode _currentNode;

        private readonly MyHalconControl _halconControl;
        private readonly HWindow _hwin;
        /// <summary>
        /// 矩形左上角
        /// </summary>
        private double _row1;
        /// <summary>
        /// 矩形左上角
        /// </summary>
        private double _column1;
        /// <summary>
        /// 矩形右下角
        /// </summary>
        private double _row2;
        /// <summary>
        /// 矩形右下角
        /// </summary>
        private double _column2;

        private HImage iii;

        public CreateTemplatelFrm(FlowNode lastNode, FlowNode currentNode)
        {
            InitializeComponent();
            this._lastNode = lastNode;
            this._currentNode = currentNode;
        }

        public CreateTemplatelFrm(MyHalconControl control)
        {
            InitializeComponent();
            _halconControl = control;
            _hwin = control.HalconWindow;
        }

        /// <summary>
        /// 加载模板图像
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateModelFrm_Load(object sender, EventArgs e)
        {
            //hv_Handle = myHalconControl1.HalconWindow;
            //myHalconControl1.DisplayImage(_lastNode.Output);

        }

        private void Select_btn_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = "匹配图像|*.png;*.jpg;*.bmp"
            };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //hw.DisplayImage(new HImage(ofd.FileName));
            }
        }

        private void Gather_btn_Click(object sender, EventArgs e)
        {

        }

        private void RectangleROI_btn_Click(object sender, EventArgs e)
        {
            _halconControl.Focus();

            _hwin.SetColor("red");
            _hwin.SetDraw("margin");

            _hwin.DrawRectangle1(out _row1, out _column1, out _row2, out _column2);
            _hwin.DispRectangle1(_row1, _column1, _row2, _column2);
        }

        private void DeleteROI_btn_Click(object sender, EventArgs e)
        {
            _hwin.ClearRectangle((int)_row1, (int)_column1, (int)_row2, (int)_column2);
            _hwin.ClearWindow();
        }

        private void Edit_btn_Click(object sender, EventArgs e)
        {
            _hwin.DispObj(iii);
        }

        private void CutOut_btn_Click(object sender, EventArgs e)
        {

        }

        private void CreateTemplate_btn_Click(object sender, EventArgs e)
        {
            HTuple hv_ModelID = null;
            // 获取设置的参数
            var numLevels = new HTuple(numLevels_cmb.Text);
            var startAngle = new HTuple(Convert.ToInt32(startangle_txt.Text));
            var extentAngle = new HTuple(Convert.ToInt32(extentAngle_txt.Text));
            var stepAngle = new HTuple(stepangle_txt.Text);
            var optiamize = optimization_txt.Text;
            var metric = metric_txt.Text;
            var constrast = new HTuple(Convert.ToInt32(contrast_txt.Text));
            var minConstrast = new HTuple(Convert.ToInt32(minContrast_txt.Text));

            var modelImg = _lastNode.Output;
            // 创建模板
            HOperatorSet.CreateShapeModel(modelImg, numLevels, startAngle.TupleRad(), extentAngle.TupleRad(), stepAngle, optiamize, metric, constrast, minConstrast, out hv_ModelID);
            // 获取对应的轮廓
            HOperatorSet.GetShapeModelContours(out HObject ho_ModelContours, hv_ModelID, 1);
            // 保存模板及轮廓
            HOperatorSet.WriteShapeModel(hv_ModelID, "model.shm");
            //HOperatorSet.WriteObject(ho_ModelContours, "model_xld");
            // 设置输入与输出
            _currentNode.Output = _lastNode.Output;
            MessageBox.Show("创建模板成功~~");
        }

        private void Complete_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
