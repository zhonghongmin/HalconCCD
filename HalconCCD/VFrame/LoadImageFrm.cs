﻿using HalconCCD.VControl;
using HalconDotNet;

namespace HalconCCD.VFrame
{
    public partial class LoadImageFrm : Form
    {
        private FlowNode currentNode;

        public LoadImageFrm(FlowNode currentNode)
        {
            InitializeComponent();
            this.currentNode = currentNode;
        }

        private void BtnFinish_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadImage_btn_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "加载图像 | *.png;*.jpg;*.bmp";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var url = dialog.FileName;
                var initImage = new HImage(url);
                // 设置设置输出
                currentNode.Output = initImage;
            }
        }
    }
}
