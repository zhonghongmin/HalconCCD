﻿using System.Windows.Forms;

namespace HalconCCD.VFrame
{
    partial class CreateTemplatelFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            createTemplate_btn = new Button();
            groupBox1 = new GroupBox();
            numLevels_cmb = new ComboBox();
            minContrast_txt = new TextBox();
            label6 = new Label();
            contrast_txt = new TextBox();
            label5 = new Label();
            metric_txt = new TextBox();
            label8 = new Label();
            optimization_txt = new TextBox();
            label4 = new Label();
            startangle_txt = new TextBox();
            label3 = new Label();
            stepangle_txt = new TextBox();
            extentAngle_txt = new TextBox();
            label7 = new Label();
            label2 = new Label();
            label1 = new Label();
            complete_btn = new Button();
            tabControl1 = new TabControl();
            create_tabp = new TabPage();
            flowLayoutPanel1 = new FlowLayoutPanel();
            groupBox2 = new GroupBox();
            create_rdo = new RadioButton();
            load_rdo = new RadioButton();
            groupBox3 = new GroupBox();
            gather_btn = new Button();
            select_btn = new Button();
            textBox1 = new TextBox();
            file_rdo = new RadioButton();
            gather_rdo = new RadioButton();
            groupBox4 = new GroupBox();
            circleROI_btn = new Button();
            deleteROI_btn = new Button();
            rectangleROI_btn = new Button();
            cutOut_btn = new Button();
            edit_btn = new Button();
            prames_tabp = new TabPage();
            tableLayoutPanel1 = new TableLayoutPanel();
            groupBox1.SuspendLayout();
            tabControl1.SuspendLayout();
            create_tabp.SuspendLayout();
            flowLayoutPanel1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            groupBox4.SuspendLayout();
            prames_tabp.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // createTemplate_btn
            // 
            createTemplate_btn.Dock = DockStyle.Fill;
            createTemplate_btn.Location = new Point(2, 298);
            createTemplate_btn.Margin = new Padding(2);
            createTemplate_btn.Name = "createTemplate_btn";
            createTemplate_btn.Size = new Size(246, 36);
            createTemplate_btn.TabIndex = 1;
            createTemplate_btn.Text = "创建匹配模板";
            createTemplate_btn.UseVisualStyleBackColor = true;
            createTemplate_btn.Click += CreateTemplate_btn_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(numLevels_cmb);
            groupBox1.Controls.Add(minContrast_txt);
            groupBox1.Controls.Add(label6);
            groupBox1.Controls.Add(contrast_txt);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(metric_txt);
            groupBox1.Controls.Add(label8);
            groupBox1.Controls.Add(optimization_txt);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(startangle_txt);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(stepangle_txt);
            groupBox1.Controls.Add(extentAngle_txt);
            groupBox1.Controls.Add(label7);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Dock = DockStyle.Fill;
            groupBox1.Location = new Point(3, 3);
            groupBox1.Margin = new Padding(2);
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new Padding(2);
            groupBox1.Size = new Size(480, 254);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            groupBox1.Text = "模板参数设置";
            // 
            // numLevels_cmb
            // 
            numLevels_cmb.FormattingEnabled = true;
            numLevels_cmb.Items.AddRange(new object[] { "auto", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" });
            numLevels_cmb.Location = new Point(89, 24);
            numLevels_cmb.Margin = new Padding(2);
            numLevels_cmb.Name = "numLevels_cmb";
            numLevels_cmb.Size = new Size(97, 25);
            numLevels_cmb.TabIndex = 2;
            numLevels_cmb.Text = "auto";
            // 
            // minContrast_txt
            // 
            minContrast_txt.Location = new Point(89, 215);
            minContrast_txt.Margin = new Padding(2);
            minContrast_txt.Name = "minContrast_txt";
            minContrast_txt.Size = new Size(97, 23);
            minContrast_txt.TabIndex = 1;
            minContrast_txt.Text = "40";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(18, 219);
            label6.Margin = new Padding(2, 0, 2, 0);
            label6.Name = "label6";
            label6.Size = new Size(68, 17);
            label6.TabIndex = 0;
            label6.Text = "最小对比度";
            // 
            // contrast_txt
            // 
            contrast_txt.Location = new Point(89, 188);
            contrast_txt.Margin = new Padding(2);
            contrast_txt.Name = "contrast_txt";
            contrast_txt.Size = new Size(97, 23);
            contrast_txt.TabIndex = 1;
            contrast_txt.Text = "25";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(18, 190);
            label5.Margin = new Padding(2, 0, 2, 0);
            label5.Name = "label5";
            label5.Size = new Size(68, 17);
            label5.TabIndex = 0;
            label5.Text = "对比度阈值";
            // 
            // metric_txt
            // 
            metric_txt.Location = new Point(89, 161);
            metric_txt.Margin = new Padding(2);
            metric_txt.Name = "metric_txt";
            metric_txt.Size = new Size(97, 23);
            metric_txt.TabIndex = 1;
            metric_txt.Text = "use_polarity";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(29, 163);
            label8.Margin = new Padding(2, 0, 2, 0);
            label8.Name = "label8";
            label8.Size = new Size(56, 17);
            label8.TabIndex = 0;
            label8.Text = "匹配指标";
            // 
            // optimization_txt
            // 
            optimization_txt.Location = new Point(89, 134);
            optimization_txt.Margin = new Padding(2);
            optimization_txt.Name = "optimization_txt";
            optimization_txt.Size = new Size(97, 23);
            optimization_txt.TabIndex = 1;
            optimization_txt.Text = "auto";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(29, 137);
            label4.Margin = new Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new Size(56, 17);
            label4.TabIndex = 0;
            label4.Text = "优化类型";
            // 
            // startangle_txt
            // 
            startangle_txt.Location = new Point(89, 53);
            startangle_txt.Margin = new Padding(2);
            startangle_txt.Name = "startangle_txt";
            startangle_txt.Size = new Size(97, 23);
            startangle_txt.TabIndex = 1;
            startangle_txt.Text = "4";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(29, 26);
            label3.Margin = new Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new Size(56, 17);
            label3.TabIndex = 0;
            label3.Text = "金字塔数";
            // 
            // stepangle_txt
            // 
            stepangle_txt.Location = new Point(89, 107);
            stepangle_txt.Margin = new Padding(2);
            stepangle_txt.Name = "stepangle_txt";
            stepangle_txt.Size = new Size(97, 23);
            stepangle_txt.TabIndex = 1;
            stepangle_txt.Text = "auto";
            // 
            // extentAngle_txt
            // 
            extentAngle_txt.Location = new Point(89, 80);
            extentAngle_txt.Margin = new Padding(2);
            extentAngle_txt.Name = "extentAngle_txt";
            extentAngle_txt.Size = new Size(97, 23);
            extentAngle_txt.TabIndex = 1;
            extentAngle_txt.Text = "360";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(29, 109);
            label7.Margin = new Padding(2, 0, 2, 0);
            label7.Name = "label7";
            label7.Size = new Size(56, 17);
            label7.TabIndex = 0;
            label7.Text = "角度步长";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(29, 82);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new Size(56, 17);
            label2.TabIndex = 0;
            label2.Text = "角度范围";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(29, 55);
            label1.Margin = new Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new Size(56, 17);
            label1.TabIndex = 0;
            label1.Text = "起始角度";
            // 
            // complete_btn
            // 
            complete_btn.Dock = DockStyle.Fill;
            complete_btn.Location = new Point(252, 298);
            complete_btn.Margin = new Padding(2);
            complete_btn.Name = "complete_btn";
            complete_btn.Size = new Size(246, 36);
            complete_btn.TabIndex = 1;
            complete_btn.Text = "完成";
            complete_btn.UseVisualStyleBackColor = true;
            complete_btn.Click += Complete_btn_Click;
            // 
            // tabControl1
            // 
            tableLayoutPanel1.SetColumnSpan(tabControl1, 2);
            tabControl1.Controls.Add(create_tabp);
            tabControl1.Controls.Add(prames_tabp);
            tabControl1.Dock = DockStyle.Fill;
            tabControl1.Location = new Point(3, 3);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new Size(494, 290);
            tabControl1.TabIndex = 3;
            // 
            // create_tabp
            // 
            create_tabp.Controls.Add(flowLayoutPanel1);
            create_tabp.Location = new Point(4, 26);
            create_tabp.Name = "create_tabp";
            create_tabp.Padding = new Padding(3);
            create_tabp.Size = new Size(486, 260);
            create_tabp.TabIndex = 0;
            create_tabp.Text = "创建模板";
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Controls.Add(groupBox2);
            flowLayoutPanel1.Controls.Add(groupBox3);
            flowLayoutPanel1.Controls.Add(groupBox4);
            flowLayoutPanel1.Dock = DockStyle.Fill;
            flowLayoutPanel1.Location = new Point(3, 3);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(480, 254);
            flowLayoutPanel1.TabIndex = 0;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(create_rdo);
            groupBox2.Controls.Add(load_rdo);
            groupBox2.Location = new Point(3, 3);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(474, 66);
            groupBox2.TabIndex = 0;
            groupBox2.TabStop = false;
            groupBox2.Text = "模板";
            // 
            // create_rdo
            // 
            create_rdo.AutoSize = true;
            create_rdo.Location = new Point(6, 16);
            create_rdo.Name = "create_rdo";
            create_rdo.Size = new Size(98, 21);
            create_rdo.TabIndex = 0;
            create_rdo.TabStop = true;
            create_rdo.Text = "从图像中创建";
            create_rdo.UseVisualStyleBackColor = true;
            // 
            // load_rdo
            // 
            load_rdo.AutoSize = true;
            load_rdo.Location = new Point(6, 39);
            load_rdo.Name = "load_rdo";
            load_rdo.Size = new Size(50, 21);
            load_rdo.TabIndex = 0;
            load_rdo.TabStop = true;
            load_rdo.Text = "加载";
            load_rdo.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(gather_btn);
            groupBox3.Controls.Add(select_btn);
            groupBox3.Controls.Add(textBox1);
            groupBox3.Controls.Add(file_rdo);
            groupBox3.Controls.Add(gather_rdo);
            groupBox3.Location = new Point(3, 75);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(474, 89);
            groupBox3.TabIndex = 1;
            groupBox3.TabStop = false;
            groupBox3.Text = "模板资源";
            // 
            // gather_btn
            // 
            gather_btn.Location = new Point(89, 49);
            gather_btn.Name = "gather_btn";
            gather_btn.Size = new Size(65, 23);
            gather_btn.TabIndex = 2;
            gather_btn.Text = "采集";
            gather_btn.UseVisualStyleBackColor = true;
            gather_btn.Click += Gather_btn_Click;
            // 
            // select_btn
            // 
            select_btn.Location = new Point(403, 22);
            select_btn.Name = "select_btn";
            select_btn.Size = new Size(65, 23);
            select_btn.TabIndex = 2;
            select_btn.Text = "选择";
            select_btn.UseVisualStyleBackColor = true;
            select_btn.Click += Select_btn_Click;
            // 
            // textBox1
            // 
            textBox1.Location = new Point(89, 20);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(308, 23);
            textBox1.TabIndex = 1;
            // 
            // file_rdo
            // 
            file_rdo.AutoSize = true;
            file_rdo.Location = new Point(9, 23);
            file_rdo.Name = "file_rdo";
            file_rdo.Size = new Size(50, 21);
            file_rdo.TabIndex = 0;
            file_rdo.TabStop = true;
            file_rdo.Text = "文件";
            file_rdo.UseVisualStyleBackColor = true;
            // 
            // gather_rdo
            // 
            gather_rdo.AutoSize = true;
            gather_rdo.Location = new Point(9, 50);
            gather_rdo.Name = "gather_rdo";
            gather_rdo.Size = new Size(74, 21);
            gather_rdo.TabIndex = 0;
            gather_rdo.TabStop = true;
            gather_rdo.Text = "采集助手";
            gather_rdo.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(circleROI_btn);
            groupBox4.Controls.Add(deleteROI_btn);
            groupBox4.Controls.Add(rectangleROI_btn);
            groupBox4.Controls.Add(cutOut_btn);
            groupBox4.Controls.Add(edit_btn);
            groupBox4.Location = new Point(3, 170);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(474, 57);
            groupBox4.TabIndex = 2;
            groupBox4.TabStop = false;
            groupBox4.Text = "模板ROI";
            // 
            // circleROI_btn
            // 
            circleROI_btn.Location = new Point(30, 22);
            circleROI_btn.Name = "circleROI_btn";
            circleROI_btn.Size = new Size(75, 23);
            circleROI_btn.TabIndex = 0;
            circleROI_btn.Text = "绘制圆形";
            circleROI_btn.UseVisualStyleBackColor = true;
            // 
            // deleteROI_btn
            // 
            deleteROI_btn.Location = new Point(192, 22);
            deleteROI_btn.Name = "deleteROI_btn";
            deleteROI_btn.Size = new Size(75, 23);
            deleteROI_btn.TabIndex = 0;
            deleteROI_btn.Text = "删除ROI";
            deleteROI_btn.UseVisualStyleBackColor = true;
            deleteROI_btn.Click += DeleteROI_btn_Click;
            // 
            // rectangleROI_btn
            // 
            rectangleROI_btn.Location = new Point(111, 22);
            rectangleROI_btn.Name = "rectangleROI_btn";
            rectangleROI_btn.Size = new Size(75, 23);
            rectangleROI_btn.TabIndex = 0;
            rectangleROI_btn.Text = "绘制矩形";
            rectangleROI_btn.UseVisualStyleBackColor = true;
            rectangleROI_btn.Click += RectangleROI_btn_Click;
            // 
            // cutOut_btn
            // 
            cutOut_btn.Location = new Point(354, 22);
            cutOut_btn.Name = "cutOut_btn";
            cutOut_btn.Size = new Size(75, 23);
            cutOut_btn.TabIndex = 0;
            cutOut_btn.Text = "截取模板图像";
            cutOut_btn.UseVisualStyleBackColor = true;
            cutOut_btn.Click += CutOut_btn_Click;
            // 
            // edit_btn
            // 
            edit_btn.Location = new Point(273, 22);
            edit_btn.Name = "edit_btn";
            edit_btn.Size = new Size(75, 23);
            edit_btn.TabIndex = 0;
            edit_btn.Text = "修改模板";
            edit_btn.UseVisualStyleBackColor = true;
            edit_btn.Click += Edit_btn_Click;
            // 
            // prames_tabp
            // 
            prames_tabp.Controls.Add(groupBox1);
            prames_tabp.Location = new Point(4, 26);
            prames_tabp.Name = "prames_tabp";
            prames_tabp.Padding = new Padding(3);
            prames_tabp.Size = new Size(486, 260);
            prames_tabp.TabIndex = 1;
            prames_tabp.Text = "模板参数";
            prames_tabp.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(createTemplate_btn, 0, 1);
            tableLayoutPanel1.Controls.Add(tabControl1, 0, 0);
            tableLayoutPanel1.Controls.Add(complete_btn, 1, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel1.Size = new Size(500, 336);
            tableLayoutPanel1.TabIndex = 4;
            // 
            // CreateTemplatelFrm
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(500, 336);
            Controls.Add(tableLayoutPanel1);
            Margin = new Padding(2);
            Name = "CreateTemplatelFrm";
            StartPosition = FormStartPosition.CenterParent;
            Text = "创建模板窗口";
            Load += CreateModelFrm_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            tabControl1.ResumeLayout(false);
            create_tabp.ResumeLayout(false);
            flowLayoutPanel1.ResumeLayout(false);
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox4.ResumeLayout(false);
            prames_tabp.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private Button createTemplate_btn;
        private GroupBox groupBox1;
        private TextBox optimization_txt;
        private Label label4;
        private TextBox startangle_txt;
        private Label label3;
        private TextBox extentAngle_txt;
        private Label label2;
        private Label label1;
        private TextBox minContrast_txt;
        private Label label6;
        private TextBox contrast_txt;
        private Label label5;
        private Button complete_btn;
        private ComboBox numLevels_cmb;
        private TextBox stepangle_txt;
        private Label label7;
        private TextBox metric_txt;
        private Label label8;
        private TabControl tabControl1;
        private TabPage prames_tabp;
        private TableLayoutPanel tableLayoutPanel1;
        private TabPage create_tabp;
        private FlowLayoutPanel flowLayoutPanel1;
        private GroupBox groupBox4;
        private Button circleROI_btn;
        private Button edit_btn;
        private Button cutOut_btn;
        private Button rectangleROI_btn;
        private Button deleteROI_btn;
        private GroupBox groupBox3;
        private RadioButton file_rdo;
        private RadioButton gather_rdo;
        private TextBox textBox1;
        private Button select_btn;
        private Button gather_btn;
        private GroupBox groupBox2;
        private RadioButton create_rdo;
        private RadioButton load_rdo;
    }
}