using HalconCCD.Camera.Hosting;
using HalconCCD.Camera.Original;
using HalconCCD.VControl;
using HalconCCD.VFrame;
using HalconCCD.VHelper;
using HalconDotNet;
using MVSDK;
using System.Drawing.Drawing2D;
using CameraHandle = System.Int32;

namespace HalconCCD
{
    public partial class MainFrm : Form
    {
        private Label toolLabel;
        private bool isMoving;
        private Point startPoint;
        private DrawState drawState = DrawState.Noramal;
        private FlowNode node1;
        private int nodeNo = 0;
        private FlowNode node2;

        private ImageAcquisition _cam02;

        public MainFrm()
        {
            InitializeComponent();
        }

        private void HalConnect_tsm_Click(object sender, EventArgs e)
        {
            _cam02 = new ImageAcquisition();
            _cam02.ConnectAsync();
        }

        private void HalDisconnect_tsm_Click(object sender, EventArgs e)
        {
            _cam02.Disconnect();
        }

        private void MvConnect_tsm_Click(object sender, EventArgs e)
        {
            //MindVison.Connect(myhctl.Handle);
            //MindVison.CyclicAcquisition(myhctl.HalconWindow);
        }

        private void SinglePic_tsm_Click(object sender, EventArgs e)
        {
            _cam02.DisplaySingle(myhctl);
        }

        private void MultiplePic_tsm_Click(object sender, EventArgs e)
        {
            _cam02.DisplayCyclic(myhctl);
        }

        private void ImageHandle_btn_Click(object sender, EventArgs e)
        {
            ImgHandle_panel.Visible = !ImgHandle_panel.Visible;
        }

        private void TemplateMatch_btn_Click(object sender, EventArgs e)
        {
            TemplateMatch_panel.Visible = !TemplateMatch_panel.Visible;
        }


        private void Tool_label_MouseDown(object sender, MouseEventArgs e)
        {
            if (sender is not Label toolLab)
            {
                return;
            }
            toolLab.DoDragDrop(sender, DragDropEffects.Copy | DragDropEffects.Move);

            switch (toolLab.Name)
            {
                case "createTemplate_lab":
                    var ct = new CreateTemplatelFrm(myhctl);
                    ct.Show(this);
                    break;
                case "drawROI_lab":
                    var drf = new DrawROIFrm(null,null);
                    drf.Show(this);
                    break;
                case "templateMatch_lab":
                    var mrf = new modelResultFrm(null, null);
                    mrf.Show(this);
                    break;
                default:
                    break;
            }
        }

        private void FlowList_panel_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(Label)))
            {
                e.Effect = DragDropEffects.Copy | DragDropEffects.Move;
            }
        }

        private void FlowList_panel_DragDrop(object sender, DragEventArgs e)
        {
            var container = sender as Control;
            var flowNode = new FlowNode();
            MouseEventHelper.RegistryMouseEvent(flowNode, NodeMouseDown, MouseEventName.MouseDown);
            MouseEventHelper.RegistryMouseEvent(flowNode, NodeMouseMove, MouseEventName.MouseMove);
            MouseEventHelper.RegistryMouseEvent(flowNode, NodeMouseUP, MouseEventName.MouseUp);
            MouseEventHelper.RegistryMouseEvent(flowNode, NodeClick, MouseEventName.MouseDown);

            flowNode.NodeName = toolLabel.Text;

            flowNode.Location = container.PointToClient(new Point(e.X, e.Y));

            container.Controls.Add(flowNode);
        }

        private void NodeMouseDown(object sender, MouseEventArgs e)
        {
            // 标识控件准备移动
            isMoving = true;
            // 记录当前鼠标位置
            startPoint = new Point(e.X, e.Y);

        }

        private void NodeMouseMove(object sender, MouseEventArgs e)
        {
            if (isMoving && e.Button == MouseButtons.Left)
            {
                var control = sender as Control;
                control.Cursor = Cursors.NoMove2D;
                var node = control.Parent;
                // 获取控件的偏移量
                int left = node.Left + (e.X - startPoint.X);
                int top = node.Top + (e.Y - startPoint.Y);
                // 获取控件本身的宽和高
                int width = node.Width;
                int height = node.Height;
                // 动态获取流程编辑区域的大小
                var rect = node.Parent.ClientRectangle;
                // 判断拖拽的过程中不能超出边界
                left = left < 0 ? 0 : (left + width > rect.Width) ? rect.Width - width : left;
                top = top < 0 ? 0 : (top + height > rect.Height) ? rect.Height - height : top;
                // 设置控件新偏移量
                node.Left = left;
                node.Top = top;
                // 强制刷新流程编辑区域
                FlowList_panel.Invalidate();
            }
        }

        private void NodeMouseUP(object sender, MouseEventArgs e)
        {
            if (isMoving)
            {
                var control = sender as Control;
                control.Cursor = Cursors.Default;
                isMoving = false;
            }
        }

        enum DrawState
        {
            Noramal, // 正常状态
            DrawLine // 连线状态
        }

        private void 连线ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drawState = DrawState.DrawLine;
        }

        private void NodeClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // 判断是否为连接线状态
                if (drawState == DrawState.DrawLine)
                {
                    var control = sender as Control;
                    if (nodeNo == 0)
                    {
                        if (control.Parent != null)
                        {
                            // 获取连线点一个节点
                            node1 = (FlowNode)control.Parent;
                            nodeNo = 1;
                        }
                        else
                        {
                            MessageBox.Show("请选择一个流程节点");
                        }
                    }
                    else if (nodeNo == 1)
                    {
                        // 绘制两个节点连线操作
                        if (control.Parent != null)
                        {
                            node2 = (FlowNode)control.Parent;
                            // 判断点击是否为同一个节点
                            if (!node1.Equals(node2))
                            {
                                // 设置定关系
                                node1.NextNode = node2.NodeID;
                                node2.LastNode = node1.NodeID;

                                // 找到了第二个节点，需要把nodeNo重置为0，为下一次连线做准备
                                nodeNo = 0;
                                // 绘制连线
                                DrawPointToPointLine(node1, node2);
                                // 恢复正常绘制状态
                                drawState = DrawState.Noramal;
                            }
                        }
                        else
                        {
                            MessageBox.Show("请选择需要连线目标节点");
                        }
                    }
                }
            }
        }

        enum LineForward
        {
            L_R,
            R_L,
            U_D,
            D_U
        }

        private void DrawPointToPointLine(FlowNode ctl1, FlowNode ctl2)
        {
            // 定义2个点对象变量
            Point p1, p2;
            //左到右
            if (Math.Abs(ctl2.Location.X - ctl1.Location.X) > Math.Abs(ctl2.Location.Y - ctl1.Location.Y) && ctl2.Location.X >= ctl1.Location.X)
            {
                p1 = new Point(ctl1.Location.X + ctl1.Width + 1, ctl1.Location.Y + ctl1.Height / 2);
                p2 = new Point(ctl2.Location.X - 1, ctl2.Location.Y + ctl2.Height / 2);
                DrawJoinLine(p1, p2, LineForward.L_R);
            }
            //右到左
            else if (Math.Abs(ctl2.Location.X - ctl1.Location.X) > Math.Abs(ctl2.Location.Y - ctl1.Location.Y) && ctl2.Location.X < ctl1.Location.X)
            {
                p1 = new Point(ctl1.Location.X - 1, ctl1.Location.Y + ctl1.Height / 2);
                p2 = new Point(ctl2.Location.X + ctl2.Width + 1, ctl2.Location.Y + ctl2.Height / 2);
                DrawJoinLine(p1, p2, LineForward.R_L);
            }
            //上到下
            else if (Math.Abs(ctl2.Location.X - ctl1.Location.X) <= Math.Abs(ctl2.Location.Y - ctl1.Location.Y) && ctl2.Location.Y >= ctl1.Location.Y)
            {

                p1 = new Point(ctl1.Location.X + ctl1.Width / 2, ctl1.Location.Y + ctl1.Height + 1);
                p2 = new Point(ctl2.Location.X + ctl1.Width / 2, ctl2.Location.Y - 1);
                DrawJoinLine(p1, p2, LineForward.U_D);
            }
            //下到上
            else if (Math.Abs(ctl2.Location.X - ctl1.Location.X) < Math.Abs(ctl2.Location.Y - ctl1.Location.Y) && ctl2.Location.Y < ctl1.Location.Y)
            {
                p1 = new Point(ctl1.Location.X + ctl1.Width / 2, ctl1.Location.Y - 1);
                p2 = new Point(ctl2.Location.X + ctl2.Width / 2, ctl2.Location.Y + ctl2.Height + 1);
                DrawJoinLine(p1, p2, LineForward.D_U);

            }
        }

        private void DrawJoinLine(Point p1, Point p2, LineForward forward)
        {
            // GDI

            Graphics g = FlowList_panel.CreateGraphics();
            g.SmoothingMode = SmoothingMode.HighQuality;
            Color color = Color.DarkRed;
            Pen p = new Pen(color, 5);
            p.DashStyle = DashStyle.Solid;
            p.StartCap = LineCap.Round;
            p.EndCap = LineCap.ArrowAnchor;
            p.LineJoin = LineJoin.Round;
            Point inflectPoint1;
            Point inflectPoint2;
            if (forward == LineForward.L_R || forward == LineForward.R_L)
            {
                inflectPoint1 = new Point((p1.X + p2.X) / 2, p1.Y);
                inflectPoint2 = new Point((p1.X + p2.X) / 2, p2.Y);

            }
            else
            {
                inflectPoint1 = new Point(p1.X, (p1.Y + p2.Y) / 2);
                inflectPoint2 = new Point(p2.X, (p1.Y + p2.Y) / 2);
            }

            Point[] points = new Point[] { p1, inflectPoint1, inflectPoint2, p2 };

            g.DrawLines(p, points);

        }

        private void FlowList_panel_Paint(object sender, PaintEventArgs e)
        {
            var control = sender as Control;
            DrawAllLines(control);
        }

        private void DrawAllLines(Control control)
        {
            // 找出流程编辑器上所有流程控件
            foreach (Control ctl1 in control.Controls)
            {
                if (ctl1 is FlowNode)
                {
                    var fn1 = (FlowNode)ctl1;
                    foreach (Control ctl2 in control.Controls)
                    {
                        if (ctl2 is FlowNode)
                        {
                            var fn2 = (FlowNode)ctl2;
                            // 判断有从属关系的才进行连线
                            if (fn1.NextNode != null && fn2.NodeID == fn1.NextNode)
                            {
                                DrawPointToPointLine(fn1, fn2);
                            }
                        }
                    }
                }
            }
        }

        private void 单次执行ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            

            //var tool = new FlowControlTool(myhctl, FlowList_panel);
            //Task.Run(() =>
            //{
            //    tool.FlowRun();
            //});
        }

        private void label9_Click(object sender, EventArgs e)
        {


        }

        private void SetCamera_tsm_Click(object sender, EventArgs e)
        {
            var csf = new CameraSettingFrm();
            csf.Show(this);
        }

        private void Processor_btn_Click(object sender, EventArgs e)
        {
            if (processor_grp.Visible == false)
            {
                tableLayoutPanel1.ColumnStyles[1].Width = 100F;
            }
            else
            {
                tableLayoutPanel1.ColumnStyles[1].Width = 0F;
            }
            processor_grp.Visible = !processor_grp.Visible;
        }
    }
}