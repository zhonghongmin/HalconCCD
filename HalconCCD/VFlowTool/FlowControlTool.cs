﻿using HalconDotNet;

namespace HalconCCD.VControl
{
    /// <summary>
    /// 流程控制及执行工具类
    /// </summary>
    public class FlowControlTool
    {
        private System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();
        private MyHalconControl hctl;
        private Control flowContainer;
        private HWindow windowId;

        // 定义存储所有流程节点的集合
        private List<FlowNode> flowControlList = new List<FlowNode>();
        private HTuple hv_ModelID;
        private HObject ho_ModelContours;

        int index = 0;
        private HObject singleImg;
        private HObject[] multiImg;
        private HDrawingObject rect;

        public FlowControlTool(MyHalconControl hctl, Control flowContainer)
        {
            this.hctl = hctl;
            this.flowContainer = flowContainer;
            windowId = hctl.HalconWindow;
        }

        /// <summary>
        /// 流程运行的方法
        /// </summary>
        public void FlowRun()
        {
            windowId.ClearWindow();
            singleImg = null;
            multiImg = null;
            // 找到流程容器中所有的流程节点对象并存入集合中
            foreach (Control control in flowContainer.Controls)
            {
                if (control is FlowNode)
                {
                    var node = (control as FlowNode);
                    node.LightColor = Color.LightGray;
                    // 存在到集合中
                    flowControlList.Add(node);
                }
            }
            // 找到整个流程的开始节点(没有赋值父ID)
            var startNode = GetStartNode();
            // 执行当前一个节点的流程并获取下一个节点继续执行(递归)
            RecursionAction(startNode);

        }

        /// <summary>
        /// 递归执行整套流程的所有节点功能
        /// </summary>
        /// <param name="startNode"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void RecursionAction(FlowNode startNode)
        {
            Thread.Sleep(1000);
            if (startNode == null)
            {
                MessageBox.Show("请添加流程节点后再执行");
                return;
            }
            var title = startNode.NodeName;

            try
            {
                // 先执行当前节点功能
                switch (title)
                {
                    case "加载图像":
                        var img = startNode.Output;
                        hctl.BeginInvoke(() =>
                        {
                            startNode.LightColor = Color.LimeGreen;
                            hctl.DisplayImage(img);
                        });

                        break;
                    case "绘制矩形ROI":
                        startNode.LightColor = Color.LimeGreen;
                        // 获取绘制矩形的数据
                        var roiData = startNode.RoiOutput;
                        windowId.SetColor("red");
                        windowId.SetDraw("margin");

                        hctl.BeginInvoke(() =>
                        {
                            startNode.LightColor = Color.LimeGreen;
                            windowId.DispRectangle1(roiData[0], roiData[1], roiData[2], roiData[3]);
                        });
                        break;
                    case "创建模板":
                        startNode.LightColor = Color.LimeGreen;
                        var modelImg = startNode.Output;

                        hctl.BeginInvoke(() =>
                        {
                            startNode.LightColor = Color.LimeGreen;
                            hctl.DisplayImage(modelImg);
                        });
                        break;
                    case "模板匹配":
                        startNode.LightColor = Color.LimeGreen;
                        // 获取匹配的资源
                        // 单图，多图

                        hctl.BeginInvoke(() =>
                        {
                            singleImg = startNode.Output;
                            multiImg = startNode.Outputs;
                            if (singleImg != null)
                            {
                                ModelMath(singleImg);
                            }
                            else
                            {
                                MathModel(multiImg);
                            }
                        });


                        break;
                }
            }
            catch (Exception)
            {

                // 写入失败日志，或者记录异常
            }

            // 判断是否有下一个子节点，如果有直接执行下一个子节点操作，没有就终止执行
            if (string.IsNullOrEmpty(startNode.NextNode))
            {
                return;
            }
            // 获取下一个流程节点
            var nextNode = GetNextNode(startNode);
            // 递归执行
            RecursionAction(nextNode);
        }

        /// <summary>
        /// 根据操作匹配
        /// </summary>
        /// <param name="multiImg"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void MathModel(HObject[] multiImg)
        {
            _timer.Start();
            _timer.Interval = 1000;
            _timer.Tick += _timer_Tick;
            _timer.Tag = multiImg;
            HOperatorSet.DispText(hctl.HalconWindow, "多图匹配....",
                "window", 12, 12, "red", new HTuple(), new HTuple());
        }

        /// <summary>
        /// 多图匹配
        /// </summary>
        private void _timer_Tick(object sender, EventArgs e)
        {
            var imgs = _timer.Tag as HObject[];
            HOperatorSet.DispText(hctl.HalconWindow, "",
                "window", 12, 12, "green", new HTuple(), new HTuple());
            // 获取测量的图像
            if (imgs.Length > 0)
            {
                // 获取需要处理的图片
                var s_img = imgs[index++];
                // 匹配
                ModelMath(s_img);

                if (index == imgs.Length)
                {
                    _timer.Stop();
                    index = 0;
                    windowId.ClearWindow();
                    HOperatorSet.DispText(windowId, "多图匹配测试结束",
                        "window", 12, 12, "black", new HTuple(), new HTuple());
                }
            }
        }

        /// <summary>
        /// 根据图像匹配
        /// </summary>
        /// <param name="L_Image"></param>
        private void ModelMath(HObject L_Image)
        {
            // 读取模板ID及轮廓
            HOperatorSet.ReadShapeModel("model.shm", out hv_ModelID);
            HOperatorSet.ReadObject(out ho_ModelContours, "model_xld");

            // 通过模板ID定位模板图像位置及角度
            HOperatorSet.FindShapeModel(L_Image, hv_ModelID, 0, (new HTuple(360)).TupleRad()
            , 0.5, 1, 0.5, "least_squares", 0, 0.7, out HTuple hv_Row1, out HTuple hv_Column1,
            out HTuple hv_Angle, out HTuple hv_Score);
            if (hv_Score.Type == HTupleType.EMPTY)
            {
                hctl.DisplayImage(L_Image);
                HOperatorSet.DispText(windowId, "匹配失败", "window", 12, 12, "red", new HTuple(), new HTuple());
                return;
            }
            // 创建一个齐次矩阵
            HOperatorSet.HomMat2dIdentity(out HTuple hv_HomMat2DIdentity);
            //模板仿射变换
            HOperatorSet.HomMat2dTranslate(hv_HomMat2DIdentity, hv_Row1, hv_Column1,
                out HTuple hv_HomMat2DTranslate);
            // 旋转
            HOperatorSet.HomMat2dRotate(hv_HomMat2DTranslate, hv_Angle, hv_Row1, hv_Column1,
                out HTuple hv_HomMat2DRotate);
            // 仿射变换轮廓
            HOperatorSet.AffineTransContourXld(ho_ModelContours, out HObject ho_ContoursAffineTrans,
                hv_HomMat2DRotate);
            // 显示结果
            HOperatorSet.SetColor(windowId, "red");
            HOperatorSet.SetLineWidth(windowId, 2);
            hctl.DisplayImage(L_Image);
            HOperatorSet.DispObj(ho_ContoursAffineTrans, windowId);
            // 获取轮廓中心
            HOperatorSet.AreaCenterXld(ho_ContoursAffineTrans, out HTuple Area, out HTuple xldRow, out HTuple xldColumn, out HTuple _);
            HOperatorSet.SetColor(windowId, "green");
            HOperatorSet.SetLineWidth(windowId, 1);
            // 绘制十字准星
            HOperatorSet.DispCross(windowId, xldRow, xldColumn, 60, 0);
            HOperatorSet.DispText(windowId, "匹配坐标：(row=" + hv_Row1.TupleString(".2f") + ",column=" + hv_Column1.TupleString(".2f"),
                "window", 12, 12, "red", new HTuple(), new HTuple());

        }

        /// <summary>
        /// 获取当前执行流程的下一个流程节点
        /// 传入节点的子节点ID == 搜索的节点的ID （下一个节点）
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private FlowNode GetNextNode(FlowNode startNode)
        {
            var childId = startNode.NextNode;

            foreach (FlowNode fc in flowControlList)
            {
                if (childId.Equals(fc.NodeID))
                {
                    return fc;
                }
            }

            return null;
        }

        /// <summary>
        /// 获取整个流程的开始节点
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private FlowNode GetStartNode()
        {
            foreach (FlowNode fc in flowControlList)
            {
                if (string.IsNullOrEmpty(fc.LastNode))
                {
                    return fc;
                }
            }

            return null;
        }
    }
}
