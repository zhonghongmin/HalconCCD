﻿using HalconDotNet;
using MVSDK;
using CameraHandle = System.Int32;

namespace HalconCCD.Camera.Original
{

    /// <summary>
    /// 迈德威视
    /// </summary>
    public class MindVison
    {

        private HWindow _windowHandle;

        private IntPtr m_Grabber = IntPtr.Zero;
        private CameraHandle m_hCamera = 0;
        private tSdkCameraDevInfo m_DevInfo;

        public string ImagePath { get; private set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "Image", $"{DateTime.Now:yyyyMMddHHmmss}.bmp" );

        public MindVison(nint windowHandle)
        {
            Connect(windowHandle);
        }

        public void Connect(nint windowHandle)
        {
            if (MvApi.CameraGrabber_CreateFromDevicePage(out m_Grabber) == CameraSdkStatus.CAMERA_STATUS_SUCCESS)
            {
                MvApi.CameraGrabber_GetCameraDevInfo(m_Grabber, out m_DevInfo);
                MvApi.CameraGrabber_GetCameraHandle(m_Grabber, out m_hCamera);
                MvApi.CameraCreateSettingPage(m_hCamera, windowHandle, m_DevInfo.acFriendlyName, null, (IntPtr)0, 0);

                MvApi.CameraGrabber_SetRGBCallback(m_Grabber, new pfnCameraGrabberFrameCallback(CameraGrabberFrameCallback), IntPtr.Zero);

                MvApi.CameraGrabber_SetSaveImageCompleteCallback(m_Grabber, new pfnCameraGrabberSaveImageComplete(CameraGrabberSaveImageComplete), IntPtr.Zero);

            }
        }

        public void CyclicAcquisition(HWindow windowHandle)
        {
            _windowHandle = windowHandle;
        }

        public void CyclicAcquisition(IntPtr displayImageHandle)
        {

            if (m_Grabber == IntPtr.Zero)
            {
                return;
            }
            //// 黑白相机设置ISP输出灰度图像
            //// 彩色相机ISP默认会输出BGR24图像
            MvApi.CameraGetCapability(m_hCamera, out tSdkCameraCapbility cap);
            if (cap.sIspCapacity.bMonoSensor != 0)
            {
                MvApi.CameraSetIspOutFormat(m_hCamera, (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_MONO8);
            }

            MvApi.CameraGrabber_SetHWnd(m_Grabber, displayImageHandle);//将相机图像展示到图像窗口句柄

            MvApi.CameraGrabber_StartLive(m_Grabber);
        }

        public void StopAcquisition()
        {
            if (m_Grabber != IntPtr.Zero)
                MvApi.CameraGrabber_StopLive(m_Grabber);
        }

        public void SaveImage(string path)
        {
            ImagePath = path;
            SaveImage();
        }

        public void SaveImage()
        {
            if (m_Grabber != IntPtr.Zero)
                MvApi.CameraGrabber_SaveImageAsync(m_Grabber);
        }

        public void CameraSet()
        {
            if (m_Grabber != IntPtr.Zero)
                MvApi.CameraShowSettingPage(m_hCamera, 1);
        }

        public void Disconnect()
        {
            MvApi.CameraGrabber_Destroy(m_Grabber);
        }

        private void CameraGrabberFrameCallback(IntPtr Grabber, IntPtr pFrameBuffer, ref tSdkFrameHead pFrameHead, IntPtr Context)
        {
            if (_windowHandle == null)
            {
                return;
            }

            HObject imageRaw;

            try
            {
                if (pFrameHead.uiMediaType == (uint)MVSDK.emImageFormat.CAMERA_MEDIA_TYPE_MONO8)
                {
                    HOperatorSet.GenImage1(out imageRaw, "byte", pFrameHead.iWidth, pFrameHead.iHeight, pFrameBuffer);
                }
                else
                {
                    throw new HalconException("Image format is not supported!!");
                }

                HOperatorSet.MirrorImage(imageRaw, out HObject imageMirror, "row");
                HOperatorSet.DispObj(imageMirror, _windowHandle);
                imageRaw.Dispose();
            }
            catch
            {
                throw new HalconException();
            }
        }


        private void CameraGrabberSaveImageComplete(IntPtr Grabber, IntPtr Image, CameraSdkStatus Status, IntPtr Context)
        {
            if (Image != IntPtr.Zero)
            {
                MvApi.CameraImage_SaveAsBmp(Image, ImagePath);
            }

            MvApi.CameraImage_Destroy(Image);
        }
    }
}
