﻿using HalconCCD.VControl;
using HalconDotNet;
using System;

namespace HalconCCD.Camera.Hosting
{
    /// <summary>
    /// Halcon托管相机执行操作
    /// </summary>
    public class ImageAcquisition
    {
        private HTuple _acqHandle;

        private bool _isBrab;

        /// <summary>
        /// 连接句柄
        /// </summary>
        public HTuple AcqHandle { get => _acqHandle; }

        public void ConnectAsync()
        {
            HOperatorSet.OpenFramegrabber("GigEVision2", 0, 0, 0, 0, 0, 0, "progressive", -1, "default", -1, "false", "default", "CAM02", 0, -1, out _acqHandle);
        }

        public void Disconnect()
        {
            HOperatorSet.CloseFramegrabber(_acqHandle);
        }

        public void DisplaySingle(MyHalconControl win)
        {
            HOperatorSet.GrabImage(out HObject image, _acqHandle);
            win.DisplayImage(image);

            var m = new Matching();
            m.WindowHandle = win.HalconWindow;
            m.TemplateInit();

            m.TemplateMatch(image);
        }

        public void DisplayCyclic(MyHalconControl win)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    DisplaySingle(win);
                }
            });
        }

        public void DisplaySingle2(MyHalconControl win)
        {
            _isBrab = false;
            HOperatorSet.GrabImageAsync(out HObject image, _acqHandle, -1);
            win.DisplayImage(image);
        }

        public void DisplayCyclic2(MyHalconControl win)
        {
            _isBrab = true;
            Task.Run(() =>
            {
                while (_isBrab)
                {
                    HOperatorSet.GrabImageAsync(out HObject image, _acqHandle, -1);
                    win.DisplayImage(image);
                }
            });
        }

        public void StopGrab()
        {
            _isBrab = false;
        }
    }
}
