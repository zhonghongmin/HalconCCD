﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalconCCD.Camera.Hosting
{
    public class Matching
    {
        // Local iconic variables 
        private HObject ho_ModelContours;
        private HObject ho_TransContours;
        public HObject ho_Image;
        public HObject ho_rectifiedImage;

        // Local control variables 
        private HTuple hv_AcqHandle;

        /// <summary>
        /// 模板ID
        /// </summary>
        private HTuple hv_ModelID;

        /// <summary>
        /// 参考点行坐标
        /// </summary>
        private HTuple hv_RefRow;

        /// <summary>
        /// 参考点列坐标
        /// </summary>
        private HTuple hv_RefColumn;

        /// <summary>
        /// 齐次变换矩阵
        /// </summary>
        private HTuple hv_HomMat2D;

        /// <summary>
        /// 找到匹配实例的行坐标
        /// </summary>
        private HTuple hv_Row;

        /// <summary>
        /// 找到匹配实例的列坐标
        /// </summary>
        private HTuple hv_Column;

        /// <summary>
        /// 找到匹配实例的角度
        /// </summary>
        private HTuple hv_Angle;

        /// <summary>
        /// 匹配的分数
        /// </summary>
        private HTuple hv_Score;

        /// <summary>
        /// 对齐操作的齐次变换矩阵
        /// </summary>
        private HTuple hv_AlignmentHomMat2D;

        /// <summary>
        /// 校正后的图像
        /// </summary>
        private HTuple hv_RectificationHomMat2D;

        /// <summary>
        /// 采集句柄
        /// </summary>
        public HTuple AcqHandle { get; set; }

        /// <summary>
        /// 绘制区域的窗口句柄
        /// </summary>
        public HWindow WindowHandle { get; set; }

        public event Action MatchComplete;

        public Matching()
        {
            // Local control variables
            hv_ModelID = new HTuple();
            hv_RefRow = new HTuple();
            hv_RefColumn = new HTuple();
            hv_HomMat2D = new HTuple();
            hv_Row = new HTuple();
            hv_Column = new HTuple();
            hv_Angle = new HTuple();
            hv_Score = new HTuple();
            hv_AlignmentHomMat2D = new HTuple();
            hv_RectificationHomMat2D = new HTuple();

            // Initialize local and output iconic variables 
            HOperatorSet.GenEmptyObj(out ho_ModelContours);
            HOperatorSet.GenEmptyObj(out ho_TransContours);
            HOperatorSet.GenEmptyObj(out ho_Image);
            HOperatorSet.GenEmptyObj(out ho_rectifiedImage);
        }

        /// <summary>
        /// 加载本地模板
        /// </summary>
        /// <param name="match_shm_path">模板路径</param>
        public void TemplateInit(string match_shm_path = @"C:\Users\zyl\Documents\MySoftware\HalconCCD\HalconCCD\bin\Debug\net8.0-windows7.0\Template\15.shm")
        {
            //
            //Matching 01: Read the shape model from file
            hv_ModelID.Dispose();
            HOperatorSet.ReadShapeModel(match_shm_path, out hv_ModelID);
            //
            //Matching 01: Get the model contour for transforming it later into the image
            ho_ModelContours.Dispose();
            HOperatorSet.GetShapeModelContours(out ho_ModelContours, hv_ModelID, 1);
            //
            //Matching 01: Set a reference position to show the model
            HOperatorSet.SmallestRectangle1Xld(ho_ModelContours, out HTuple hv_Row1, out HTuple hv_Column1, out HTuple hv_Row2, out HTuple hv_Column2);
            hv_RefRow.Dispose();
            hv_RefColumn.Dispose();
            using (var dh = new HDevDisposeHelper())
            {
                hv_RefRow = ((hv_Row2.TupleMax()) - (hv_Row1.TupleMin())) / 2;
                hv_RefColumn = ((hv_Column2.TupleMax()) - (hv_Column1.TupleMin())) / 2;
            }

            hv_HomMat2D.Dispose();
            HOperatorSet.VectorAngleToRigid(0, 0, 0, hv_RefRow, hv_RefColumn, 0, out hv_HomMat2D);
            ho_TransContours.Dispose();
            HOperatorSet.AffineTransContourXld(ho_ModelContours, out ho_TransContours, hv_HomMat2D);
            //
            //Matching 01: Display the model contours

           // HOperatorSet.SetColor(WindowHandle, "green");

           // HOperatorSet.SetDraw(WindowHandle, "margin");

           // HOperatorSet.DispObj(ho_TransContours, WindowHandle);
        }

        /// <summary>
        /// 模板匹配
        /// </summary>
        public void TemplateMatch(HObject image)
        {
            //
            //Matching 01: Find the model
            using (var dh = new HDevDisposeHelper())
            {
                hv_Row.Dispose(); hv_Column.Dispose(); hv_Angle.Dispose(); hv_Score.Dispose();
                HOperatorSet.FindShapeModel(image, hv_ModelID, (new HTuple(0)).TupleRad(), (new HTuple(360)).TupleRad(), 0.72, 0, 0.5, "least_squares", (new HTuple(2)).TupleConcat(1), 0.75, out hv_Row, out hv_Column, out hv_Angle, out hv_Score);
            }

            //
            //Matching 01: Transform the model contours into the detected positions
            //将模型轮廓转换为检测位置
           // HOperatorSet.DispObj(image, WindowHandle);

            for (HTuple hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
            {
                hv_HomMat2D.Dispose();
                HOperatorSet.HomMat2dIdentity(out hv_HomMat2D);
                using (var dh = new HDevDisposeHelper())
                {
                    HOperatorSet.HomMat2dRotate(hv_HomMat2D, hv_Angle.TupleSelect(hv_I), 0, 0, out HTuple ExpTmpOutVar_0);
                    hv_HomMat2D.Dispose();
                    hv_HomMat2D = ExpTmpOutVar_0;
                }
                using (var dh = new HDevDisposeHelper())
                {
                    HOperatorSet.HomMat2dTranslate(hv_HomMat2D, hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out HTuple ExpTmpOutVar_0);
                    hv_HomMat2D.Dispose();
                    hv_HomMat2D = ExpTmpOutVar_0;
                }
                ho_TransContours.Dispose();
                HOperatorSet.AffineTransContourXld(ho_ModelContours, out ho_TransContours, hv_HomMat2D);

                HOperatorSet.SetColor(WindowHandle, "green");

                HOperatorSet.DispObj(ho_TransContours, WindowHandle);

            }
            ////
            ////Matching 01: Code for alignment of e.g. measurements
            ////Matching 01: Calculate a hom_mat2d for each of the matching results
            //for (HTuple hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
            //{
            //    hv_AlignmentHomMat2D.Dispose();
            //    HOperatorSet.HomMat2dIdentity(out hv_AlignmentHomMat2D);
            //    using (var dh = new HDevDisposeHelper())
            //    {
            //        HOperatorSet.HomMat2dTranslate(hv_AlignmentHomMat2D, -hv_RefRow, -hv_RefColumn, out HTuple ExpTmpOutVar_0);
            //        hv_AlignmentHomMat2D.Dispose();
            //        hv_AlignmentHomMat2D = ExpTmpOutVar_0;
            //    }
            //    using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //    {
            //        HTuple ExpTmpOutVar_0;
            //        HOperatorSet.HomMat2dRotate(hv_AlignmentHomMat2D, hv_Angle.TupleSelect(hv_I), 0, 0, out ExpTmpOutVar_0);
            //        hv_AlignmentHomMat2D.Dispose();
            //        hv_AlignmentHomMat2D = ExpTmpOutVar_0;
            //    }
            //    using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //    {
            //        HTuple ExpTmpOutVar_0;
            //        HOperatorSet.HomMat2dTranslate(hv_AlignmentHomMat2D, hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out ExpTmpOutVar_0);
            //        hv_AlignmentHomMat2D.Dispose();
            //        hv_AlignmentHomMat2D = ExpTmpOutVar_0;
            //    }
            //    //Matching 01: Insert your code using the alignment here, e.g. code generated by
            //    //Matching 01: the measure assistant with the code generation option
            //    //Matching 01: 'Alignment Method' set to 'Affine Transformation'.
            //}
            ////
            ////Matching 01: Code for rectification of the image
            ////Matching 01: Calculate an inverse hom_mat2d for each of the matching results
            //for (HTuple hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
            //{
            //    hv_RectificationHomMat2D.Dispose();
            //    HOperatorSet.HomMat2dIdentity(out hv_RectificationHomMat2D);
            //    using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //    {
            //        HOperatorSet.HomMat2dTranslate(hv_RectificationHomMat2D, hv_RefRow - (hv_Row.TupleSelect(hv_I)), hv_RefColumn - (hv_Column.TupleSelect(hv_I)), out HTuple ExpTmpOutVar_0);
            //        hv_RectificationHomMat2D.Dispose();
            //        hv_RectificationHomMat2D = ExpTmpOutVar_0;
            //    }
            //    using (HDevDisposeHelper dh = new HDevDisposeHelper())
            //    {
            //        HOperatorSet.HomMat2dRotate(hv_RectificationHomMat2D, -(hv_Angle.TupleSelect(hv_I)), hv_RefRow, hv_RefColumn, out HTuple ExpTmpOutVar_0);
            //        hv_RectificationHomMat2D.Dispose();
            //        hv_RectificationHomMat2D = ExpTmpOutVar_0;
            //    }
            //    ho_rectifiedImage.Dispose();
            //    //HOperatorSet.AffineTransContourXld(ho_ModelContours, out HObject  ho_ContoursAffineTrans, hv_RectificationHomMat2D);
            //    HOperatorSet.AffineTransImage(ho_Image, out ho_rectifiedImage, hv_RectificationHomMat2D, "constant", "false");
               
            //    //
            //    //Matching 01: Insert your code using the rectified image here
            //}
        }

        public void TemplateMatch()
        {
            ho_Image.Dispose();
            HOperatorSet.GrabImage(out ho_Image, hv_AcqHandle);
            TemplateMatch(ho_Image);
        }
    }
}
