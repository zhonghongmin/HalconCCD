﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalconCCD.SlaveComputer.Data
{
    public struct Location
    {
        public int X { get; set; }

        public int Y { get; set; }

        //public int ΘZ { get; set; }
    }
}
