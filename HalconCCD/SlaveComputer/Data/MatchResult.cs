﻿using HalconCCD.SlaveComputer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalconCCD.SlaveComputer.Data
{
    public struct MatchResult
    {
        public int OKCount { get; set; }
        public Location[] OKLocations { get; set; }
    }
}
