﻿using HalconCCD.SlaveComputer.Data;
using HslCommunication;
using HslCommunication.Profinet.Omron;

namespace HalconCCD.SlaveComputer
{
    public interface IComputer
    {
        event Action<bool> Photograph;
        void Connect();
        void ResultDelivery(MatchResult result);
    }

    public class OmronPLC : IComputer
    {
        public event Action<bool> Photograph;
        private OmronHostLinkOverTcp omron;

        public async void Connect()
        {
            omron = new OmronHostLinkOverTcp("192.168.0.10", 9600);
            var result = await omron.ConnectServerAsync();
            Listener(result.IsSuccess);
        }

        public async Task DisConnectAsync()
        {
            if (omron == null)
            {
                return;
            }

            await omron.ConnectCloseAsync();
        }

        public void ResultDelivery(MatchResult result)
        {
            var num = new List<int>()
            {
                result.OKCount
            };

            for (int i = 0; i < result.OKLocations.Length; i++)
            {
                num.Add(result.OKLocations[i].X);
                num.Add(result.OKLocations[i].Y);
            }
            var r = num.ToArray<int>();

            omron.WriteAsync("D50", r);
        }

        private void Listener(bool connect)
        {
            Task.Run(async () =>
            {
                while (connect)
                {
                    try
                    {
                        OperateResult<int> result = await omron.ReadInt32Async("D0");
                        if (result.IsSuccess && result.Content == 1)
                        {
                            Photograph.Invoke(Convert.ToBoolean(1));
                        }
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
            });
        }
    }
}