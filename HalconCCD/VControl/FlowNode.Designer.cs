﻿namespace HalconCCD.VControl
{
    partial class FlowNode
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            lblLED = new Label();
            lblTitle = new Label();
            SuspendLayout();
            // 
            // lblLED
            // 
            lblLED.BackColor = SystemColors.HotTrack;
            lblLED.Dock = DockStyle.Left;
            lblLED.Font = new Font("Microsoft YaHei UI", 16F, FontStyle.Bold);
            lblLED.ForeColor = SystemColors.ActiveBorder;
            lblLED.Location = new Point(0, 0);
            lblLED.Margin = new Padding(2, 0, 2, 0);
            lblLED.Name = "lblLED";
            lblLED.Size = new Size(32, 41);
            lblLED.TabIndex = 0;
            lblLED.Text = "●";
            lblLED.TextAlign = ContentAlignment.MiddleCenter;
            lblLED.MouseDoubleClick += FlowNode_MouseDoubleClick;
            // 
            // lblTitle
            // 
            lblTitle.Dock = DockStyle.Left;
            lblTitle.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Bold);
            lblTitle.Location = new Point(32, 0);
            lblTitle.Margin = new Padding(2, 0, 2, 0);
            lblTitle.Name = "lblTitle";
            lblTitle.Size = new Size(102, 41);
            lblTitle.TabIndex = 1;
            lblTitle.Text = "标题";
            lblTitle.TextAlign = ContentAlignment.MiddleCenter;
            lblTitle.MouseDoubleClick += FlowNode_MouseDoubleClick;
            // 
            // FlowNode
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(lblTitle);
            Controls.Add(lblLED);
            Margin = new Padding(2, 2, 2, 2);
            Name = "FlowNode";
            Size = new Size(133, 41);
            MouseDoubleClick += FlowNode_MouseDoubleClick;
            ResumeLayout(false);
        }

        #endregion

        private Label lblLED;
        private Label lblTitle;
    }
}
