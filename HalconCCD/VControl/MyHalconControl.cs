﻿using HalconDotNet;

namespace HalconCCD.VControl
{
    public partial class MyHalconControl : UserControl
    {
        private HWindowControl halconCtl;

        public HWindow HalconWindow { get; private set; }

        public MyHalconControl()
        {
            InitializeComponent();
        }
        
        public void DisplayImage(HObject image)
        {
            ImgIsNotStretchDisplay(image, HalconWindow);
        }

        public void Clear()
        {
            HOperatorSet.ClearWindow(HalconWindow);
        }



        private void MyHalconControl_Load(object sender, EventArgs e)
        {
            halconCtl = new HWindowControl();
            HalconWindow = halconCtl.HalconWindow;
            // 设置布局
            halconCtl.Dock = DockStyle.Fill;
            // 添加到对应容器中
            this.Controls.Add(halconCtl);
        }

        private static void ImgIsNotStretchDisplay(HObject L_Img, HTuple Hwindow)
        {
            HOperatorSet.GetImageSize(L_Img, out var width, out var height);
            HOperatorSet.GetWindowExtents(Hwindow, out var _, out var _, out var width2, out var height2);
            HTuple hTuple = 1.0 * height2 / width2 * width;
            if (hTuple > height)
            {
                hTuple = 1.0 * (hTuple - height) / 2;
                HOperatorSet.SetPart(Hwindow, -hTuple, 0, hTuple + height, width);
            }
            else
            {
                HTuple hTuple2 = 1.0 * width2 / height2 * height;
                hTuple2 = 1.0 * (hTuple2 - width) / 2;
                HOperatorSet.SetPart(Hwindow, 0, -hTuple2, height, hTuple2 + width);
            }

            HOperatorSet.DispObj(L_Img, Hwindow);
        }
    }
}
