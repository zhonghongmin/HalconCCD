﻿using HalconCCD.VFrame;
using HalconDotNet;
using System.ComponentModel;

namespace HalconCCD.VControl
{
    public partial class FlowNode : UserControl
    {
        /// <summary>
        /// 上一个节点
        /// </summary>
        private static FlowNode _lastNode;

        /// <summary>
        /// 节点编号
        /// </summary>
        public string NodeID { get; set; }

        /// <summary>
        /// 前一个节点ID
        /// </summary>
        public string LastNode { get; set; }

        /// <summary>
        /// 下一个节点ID
        /// </summary>
        public string NextNode { get; set; }


        public HObject Input;
        public HObject Output;
        public HTuple[] InputTuple;
        public HTuple[] OutputTuple;


        [Category("流程控件属性")]
        public string NodeName { get => lblTitle.Text; set => lblTitle.Text = value; }

        [Category("流程控件属性")]
        public Color LightColor { get => lblLED.ForeColor; set => lblLED.ForeColor = value; }

        public HObject[] Outputs { get; internal set; }

        public HTuple[] RoiOutput { get; internal set; }

        public FlowNode()
        {
            InitializeComponent();
            // 初始化节点ID
            NodeID = Guid.NewGuid().ToString();
        }

        private void FlowNode_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // 获取点击的流程节点对象的标题
            var nodeTitle = this.NodeName;
            // 保存当前节点数据

            switch (nodeTitle)
            {
                case "加载图像":
                    new LoadImageFrm(this).ShowDialog();
                    _lastNode = this;
                    break;
                case "绘制矩形ROI":
                    new DrawROIFrm(_lastNode, this).ShowDialog();
                    _lastNode = this;
                    break;
                case "创建模板":
                    new CreateTemplatelFrm(_lastNode, this).ShowDialog();
                    _lastNode = this;
                    break;
                case "模板匹配":
                    new modelResultFrm(_lastNode, this).ShowDialog();
                    _lastNode = this;
                    break;
            }
        }
    }
}
