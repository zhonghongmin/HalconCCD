﻿using HalconCCD.VControl;
using HalconDotNet;

namespace HalconCCD
{
    partial class MainFrm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            groupBox1 = new GroupBox();
            flowLayoutPanel1 = new FlowLayoutPanel();
            ImageHandle_btn = new Button();
            ImgHandle_panel = new Panel();
            label4 = new Label();
            label3 = new Label();
            label2 = new Label();
            loadImage_lbl = new Label();
            TemplateMatch_btn = new Button();
            TemplateMatch_panel = new Panel();
            calibration_lab = new Label();
            templateMatch_lab = new Label();
            createTemplate_lab = new Label();
            drawROI_lab = new Label();
            label6 = new Label();
            processor_btn = new Button();
            groupBox3 = new GroupBox();
            panel3 = new Panel();
            myhctl = new MyHalconControl();
            groupBox4 = new GroupBox();
            textBox1 = new TextBox();
            FlowList_panel = new Panel();
            cm_drawJoinLine = new ContextMenuStrip(components);
            连线ToolStripMenuItem = new ToolStripMenuItem();
            processor_grp = new GroupBox();
            tableLayoutPanel1 = new TableLayoutPanel();
            menuStrip1 = new MenuStrip();
            setCamera_tsm = new ToolStripMenuItem();
            connect_tsm = new ToolStripMenuItem();
            halcon_tsm = new ToolStripMenuItem();
            halConnect_tsm = new ToolStripMenuItem();
            halDisconnect_tsm = new ToolStripMenuItem();
            mindVision_tsm = new ToolStripMenuItem();
            OpenCamera_tsm = new ToolStripMenuItem();
            CloseCamera_tsm = new ToolStripMenuItem();
            SnapPictyre_tsm = new ToolStripMenuItem();
            omronPLC_tsm = new ToolStripMenuItem();
            plcConnect_tsm = new ToolStripMenuItem();
            plcDisconnect_tsm = new ToolStripMenuItem();
            singlePic_tsm = new ToolStripMenuItem();
            multiplePic_tsm = new ToolStripMenuItem();
            singleProcess_tsm = new ToolStripMenuItem();
            multipleProcess_tsm = new ToolStripMenuItem();
            groupBox1.SuspendLayout();
            flowLayoutPanel1.SuspendLayout();
            ImgHandle_panel.SuspendLayout();
            TemplateMatch_panel.SuspendLayout();
            groupBox3.SuspendLayout();
            panel3.SuspendLayout();
            groupBox4.SuspendLayout();
            cm_drawJoinLine.SuspendLayout();
            processor_grp.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(flowLayoutPanel1);
            groupBox1.Dock = DockStyle.Fill;
            groupBox1.Location = new Point(2, 32);
            groupBox1.Margin = new Padding(2);
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new Padding(2);
            tableLayoutPanel1.SetRowSpan(groupBox1, 2);
            groupBox1.Size = new Size(248, 695);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "视觉工具箱";
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.BackColor = Color.Honeydew;
            flowLayoutPanel1.Controls.Add(ImageHandle_btn);
            flowLayoutPanel1.Controls.Add(ImgHandle_panel);
            flowLayoutPanel1.Controls.Add(TemplateMatch_btn);
            flowLayoutPanel1.Controls.Add(TemplateMatch_panel);
            flowLayoutPanel1.Controls.Add(processor_btn);
            flowLayoutPanel1.Dock = DockStyle.Fill;
            flowLayoutPanel1.Location = new Point(2, 18);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(244, 675);
            flowLayoutPanel1.TabIndex = 0;
            // 
            // ImageHandle_btn
            // 
            ImageHandle_btn.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            ImageHandle_btn.BackColor = Color.Turquoise;
            ImageHandle_btn.Font = new Font("Microsoft YaHei UI", 12F);
            ImageHandle_btn.ForeColor = SystemColors.ControlLightLight;
            ImageHandle_btn.Location = new Point(2, 2);
            ImageHandle_btn.Margin = new Padding(2);
            ImageHandle_btn.Name = "ImageHandle_btn";
            ImageHandle_btn.Size = new Size(238, 50);
            ImageHandle_btn.TabIndex = 1;
            ImageHandle_btn.Text = "图像处理工具";
            ImageHandle_btn.UseVisualStyleBackColor = false;
            ImageHandle_btn.Click += ImageHandle_btn_Click;
            // 
            // ImgHandle_panel
            // 
            ImgHandle_panel.BorderStyle = BorderStyle.FixedSingle;
            ImgHandle_panel.Controls.Add(label4);
            ImgHandle_panel.Controls.Add(label3);
            ImgHandle_panel.Controls.Add(label2);
            ImgHandle_panel.Controls.Add(loadImage_lbl);
            ImgHandle_panel.Location = new Point(2, 56);
            ImgHandle_panel.Margin = new Padding(2);
            ImgHandle_panel.Name = "ImgHandle_panel";
            ImgHandle_panel.Size = new Size(238, 187);
            ImgHandle_panel.TabIndex = 2;
            ImgHandle_panel.Visible = false;
            // 
            // label4
            // 
            label4.BackColor = Color.Azure;
            label4.BorderStyle = BorderStyle.FixedSingle;
            label4.Cursor = Cursors.Hand;
            label4.Location = new Point(29, 133);
            label4.Margin = new Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new Size(180, 30);
            label4.TabIndex = 0;
            label4.Text = "形态学处理";
            label4.TextAlign = ContentAlignment.MiddleCenter;
            label4.MouseDown += Tool_label_MouseDown;
            // 
            // label3
            // 
            label3.BackColor = Color.Azure;
            label3.BorderStyle = BorderStyle.FixedSingle;
            label3.Cursor = Cursors.Hand;
            label3.Location = new Point(29, 93);
            label3.Margin = new Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new Size(180, 30);
            label3.TabIndex = 0;
            label3.Text = "二值化图像";
            label3.TextAlign = ContentAlignment.MiddleCenter;
            label3.MouseDown += Tool_label_MouseDown;
            // 
            // label2
            // 
            label2.BackColor = Color.Azure;
            label2.BorderStyle = BorderStyle.FixedSingle;
            label2.Cursor = Cursors.Hand;
            label2.Location = new Point(29, 51);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new Size(180, 30);
            label2.TabIndex = 0;
            label2.Text = "灰度化图像";
            label2.TextAlign = ContentAlignment.MiddleCenter;
            label2.MouseDown += Tool_label_MouseDown;
            // 
            // loadImage_lbl
            // 
            loadImage_lbl.AllowDrop = true;
            loadImage_lbl.BackColor = Color.Azure;
            loadImage_lbl.BorderStyle = BorderStyle.FixedSingle;
            loadImage_lbl.Cursor = Cursors.Hand;
            loadImage_lbl.Location = new Point(29, 9);
            loadImage_lbl.Margin = new Padding(2, 0, 2, 0);
            loadImage_lbl.Name = "loadImage_lbl";
            loadImage_lbl.Size = new Size(180, 30);
            loadImage_lbl.TabIndex = 0;
            loadImage_lbl.Text = "加载图像";
            loadImage_lbl.TextAlign = ContentAlignment.MiddleCenter;
            loadImage_lbl.MouseDown += Tool_label_MouseDown;
            // 
            // TemplateMatch_btn
            // 
            TemplateMatch_btn.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            TemplateMatch_btn.BackColor = Color.Turquoise;
            TemplateMatch_btn.Font = new Font("Microsoft YaHei UI", 12F);
            TemplateMatch_btn.ForeColor = SystemColors.ControlLightLight;
            TemplateMatch_btn.Location = new Point(2, 247);
            TemplateMatch_btn.Margin = new Padding(2);
            TemplateMatch_btn.Name = "TemplateMatch_btn";
            TemplateMatch_btn.Size = new Size(238, 50);
            TemplateMatch_btn.TabIndex = 3;
            TemplateMatch_btn.Text = "模板匹配工具";
            TemplateMatch_btn.UseVisualStyleBackColor = false;
            TemplateMatch_btn.Click += TemplateMatch_btn_Click;
            // 
            // TemplateMatch_panel
            // 
            TemplateMatch_panel.BorderStyle = BorderStyle.FixedSingle;
            TemplateMatch_panel.Controls.Add(calibration_lab);
            TemplateMatch_panel.Controls.Add(templateMatch_lab);
            TemplateMatch_panel.Controls.Add(createTemplate_lab);
            TemplateMatch_panel.Controls.Add(drawROI_lab);
            TemplateMatch_panel.Controls.Add(label6);
            TemplateMatch_panel.Location = new Point(2, 301);
            TemplateMatch_panel.Margin = new Padding(2);
            TemplateMatch_panel.Name = "TemplateMatch_panel";
            TemplateMatch_panel.Size = new Size(238, 226);
            TemplateMatch_panel.TabIndex = 4;
            // 
            // calibration_lab
            // 
            calibration_lab.BackColor = Color.Azure;
            calibration_lab.BorderStyle = BorderStyle.FixedSingle;
            calibration_lab.Location = new Point(29, 179);
            calibration_lab.Name = "calibration_lab";
            calibration_lab.Size = new Size(180, 30);
            calibration_lab.TabIndex = 1;
            calibration_lab.Text = "标定";
            calibration_lab.TextAlign = ContentAlignment.MiddleCenter;
            calibration_lab.Click += label9_Click;
            // 
            // templateMatch_lab
            // 
            templateMatch_lab.BackColor = Color.Azure;
            templateMatch_lab.BorderStyle = BorderStyle.FixedSingle;
            templateMatch_lab.Cursor = Cursors.Hand;
            templateMatch_lab.Location = new Point(29, 137);
            templateMatch_lab.Margin = new Padding(2, 0, 2, 0);
            templateMatch_lab.Name = "templateMatch_lab";
            templateMatch_lab.Size = new Size(180, 30);
            templateMatch_lab.TabIndex = 0;
            templateMatch_lab.Text = "模板匹配";
            templateMatch_lab.TextAlign = ContentAlignment.MiddleCenter;
            templateMatch_lab.MouseDown += Tool_label_MouseDown;
            // 
            // createTemplate_lab
            // 
            createTemplate_lab.BackColor = Color.Azure;
            createTemplate_lab.BorderStyle = BorderStyle.FixedSingle;
            createTemplate_lab.Cursor = Cursors.Hand;
            createTemplate_lab.Location = new Point(29, 96);
            createTemplate_lab.Margin = new Padding(2, 0, 2, 0);
            createTemplate_lab.Name = "createTemplate_lab";
            createTemplate_lab.Size = new Size(180, 30);
            createTemplate_lab.TabIndex = 0;
            createTemplate_lab.Text = "创建模板";
            createTemplate_lab.TextAlign = ContentAlignment.MiddleCenter;
            createTemplate_lab.MouseDown += Tool_label_MouseDown;
            // 
            // drawROI_lab
            // 
            drawROI_lab.BackColor = Color.Azure;
            drawROI_lab.BorderStyle = BorderStyle.FixedSingle;
            drawROI_lab.Cursor = Cursors.Hand;
            drawROI_lab.Location = new Point(29, 13);
            drawROI_lab.Margin = new Padding(2, 0, 2, 0);
            drawROI_lab.Name = "drawROI_lab";
            drawROI_lab.Size = new Size(180, 30);
            drawROI_lab.TabIndex = 0;
            drawROI_lab.Text = "绘制矩形ROI";
            drawROI_lab.TextAlign = ContentAlignment.MiddleCenter;
            drawROI_lab.MouseDown += Tool_label_MouseDown;
            // 
            // label6
            // 
            label6.BackColor = Color.Azure;
            label6.BorderStyle = BorderStyle.FixedSingle;
            label6.Cursor = Cursors.Hand;
            label6.Location = new Point(29, 55);
            label6.Margin = new Padding(2, 0, 2, 0);
            label6.Name = "label6";
            label6.Size = new Size(180, 30);
            label6.TabIndex = 0;
            label6.Text = "绘制圆形ROI";
            label6.TextAlign = ContentAlignment.MiddleCenter;
            label6.MouseDown += Tool_label_MouseDown;
            // 
            // processor_btn
            // 
            processor_btn.BackColor = Color.Turquoise;
            processor_btn.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 134);
            processor_btn.ForeColor = SystemColors.ControlLightLight;
            processor_btn.Location = new Point(3, 532);
            processor_btn.Name = "processor_btn";
            processor_btn.Size = new Size(238, 50);
            processor_btn.TabIndex = 5;
            processor_btn.Text = "流程编辑器";
            processor_btn.UseVisualStyleBackColor = false;
            processor_btn.Click += Processor_btn_Click;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(panel3);
            groupBox3.Dock = DockStyle.Fill;
            groupBox3.Location = new Point(254, 32);
            groupBox3.Margin = new Padding(2);
            groupBox3.Name = "groupBox3";
            groupBox3.Padding = new Padding(2);
            groupBox3.Size = new Size(752, 345);
            groupBox3.TabIndex = 0;
            groupBox3.TabStop = false;
            groupBox3.Text = "流程执行可视化窗口";
            // 
            // panel3
            // 
            panel3.Controls.Add(myhctl);
            panel3.Dock = DockStyle.Fill;
            panel3.Location = new Point(2, 18);
            panel3.Margin = new Padding(2);
            panel3.Name = "panel3";
            panel3.Size = new Size(748, 325);
            panel3.TabIndex = 0;
            // 
            // myhctl
            // 
            myhctl.Dock = DockStyle.Fill;
            myhctl.Location = new Point(0, 0);
            myhctl.Margin = new Padding(1);
            myhctl.Name = "myhctl";
            myhctl.Size = new Size(748, 325);
            myhctl.TabIndex = 0;
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(textBox1);
            groupBox4.Dock = DockStyle.Fill;
            groupBox4.Location = new Point(254, 381);
            groupBox4.Margin = new Padding(2);
            groupBox4.Name = "groupBox4";
            groupBox4.Padding = new Padding(2);
            groupBox4.Size = new Size(752, 346);
            groupBox4.TabIndex = 0;
            groupBox4.TabStop = false;
            groupBox4.Text = "执行结果日志";
            // 
            // textBox1
            // 
            textBox1.Dock = DockStyle.Fill;
            textBox1.Location = new Point(2, 18);
            textBox1.Multiline = true;
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(748, 326);
            textBox1.TabIndex = 0;
            // 
            // FlowList_panel
            // 
            FlowList_panel.AllowDrop = true;
            FlowList_panel.BackColor = Color.LightCyan;
            FlowList_panel.ContextMenuStrip = cm_drawJoinLine;
            FlowList_panel.Dock = DockStyle.Fill;
            FlowList_panel.Location = new Point(2, 18);
            FlowList_panel.Margin = new Padding(2);
            FlowList_panel.Name = "FlowList_panel";
            FlowList_panel.Size = new Size(0, 675);
            FlowList_panel.TabIndex = 0;
            FlowList_panel.DragDrop += FlowList_panel_DragDrop;
            FlowList_panel.DragEnter += FlowList_panel_DragEnter;
            FlowList_panel.Paint += FlowList_panel_Paint;
            // 
            // cm_drawJoinLine
            // 
            cm_drawJoinLine.ImageScalingSize = new Size(24, 24);
            cm_drawJoinLine.Items.AddRange(new ToolStripItem[] { 连线ToolStripMenuItem });
            cm_drawJoinLine.Name = "cm_drawJoinLine";
            cm_drawJoinLine.Size = new Size(101, 26);
            // 
            // 连线ToolStripMenuItem
            // 
            连线ToolStripMenuItem.Name = "连线ToolStripMenuItem";
            连线ToolStripMenuItem.Size = new Size(100, 22);
            连线ToolStripMenuItem.Text = "连线";
            连线ToolStripMenuItem.Click += 连线ToolStripMenuItem_Click;
            // 
            // processor_grp
            // 
            processor_grp.Controls.Add(FlowList_panel);
            processor_grp.Dock = DockStyle.Fill;
            processor_grp.Location = new Point(254, 32);
            processor_grp.Margin = new Padding(2);
            processor_grp.Name = "processor_grp";
            processor_grp.Padding = new Padding(2);
            tableLayoutPanel1.SetRowSpan(processor_grp, 2);
            processor_grp.Size = new Size(1, 695);
            processor_grp.TabIndex = 1;
            processor_grp.TabStop = false;
            processor_grp.Text = "流程编辑";
            processor_grp.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 3;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 252F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 0F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(groupBox3, 2, 1);
            tableLayoutPanel1.Controls.Add(menuStrip1, 0, 0);
            tableLayoutPanel1.Controls.Add(groupBox4, 2, 2);
            tableLayoutPanel1.Controls.Add(groupBox1, 0, 1);
            tableLayoutPanel1.Controls.Add(processor_grp, 1, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 3;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 30F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new Size(1008, 729);
            tableLayoutPanel1.TabIndex = 2;
            // 
            // menuStrip1
            // 
            menuStrip1.AutoSize = false;
            menuStrip1.BackColor = Color.Linen;
            tableLayoutPanel1.SetColumnSpan(menuStrip1, 3);
            menuStrip1.Dock = DockStyle.Fill;
            menuStrip1.Items.AddRange(new ToolStripItem[] { setCamera_tsm, connect_tsm, singlePic_tsm, multiplePic_tsm, singleProcess_tsm, multipleProcess_tsm });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1008, 30);
            menuStrip1.TabIndex = 3;
            menuStrip1.Text = "menuStrip1";
            // 
            // setCamera_tsm
            // 
            setCamera_tsm.Font = new Font("Microsoft YaHei UI", 10.5F);
            setCamera_tsm.Name = "setCamera_tsm";
            setCamera_tsm.Size = new Size(77, 26);
            setCamera_tsm.Text = "相机设置";
            setCamera_tsm.Click += SetCamera_tsm_Click;
            // 
            // connect_tsm
            // 
            connect_tsm.DropDownItems.AddRange(new ToolStripItem[] { halcon_tsm, mindVision_tsm, omronPLC_tsm });
            connect_tsm.Font = new Font("Microsoft YaHei UI", 10.5F);
            connect_tsm.Name = "connect_tsm";
            connect_tsm.Size = new Size(49, 26);
            connect_tsm.Text = "连接";
            // 
            // halcon_tsm
            // 
            halcon_tsm.DropDownItems.AddRange(new ToolStripItem[] { halConnect_tsm, halDisconnect_tsm });
            halcon_tsm.Name = "halcon_tsm";
            halcon_tsm.Size = new Size(211, 24);
            halcon_tsm.Text = "Halcon托管相机";
            // 
            // halConnect_tsm
            // 
            halConnect_tsm.Name = "halConnect_tsm";
            halConnect_tsm.Size = new Size(106, 24);
            halConnect_tsm.Text = "连接";
            halConnect_tsm.Click += HalConnect_tsm_Click;
            // 
            // halDisconnect_tsm
            // 
            halDisconnect_tsm.Name = "halDisconnect_tsm";
            halDisconnect_tsm.Size = new Size(106, 24);
            halDisconnect_tsm.Text = "断开";
            halDisconnect_tsm.Click += HalDisconnect_tsm_Click;
            // 
            // mindVision_tsm
            // 
            mindVision_tsm.DropDownItems.AddRange(new ToolStripItem[] { OpenCamera_tsm, CloseCamera_tsm, SnapPictyre_tsm });
            mindVision_tsm.Name = "mindVision_tsm";
            mindVision_tsm.Size = new Size(211, 24);
            mindVision_tsm.Text = "MindVision直连相机";
            mindVision_tsm.Click += MvConnect_tsm_Click;
            // 
            // OpenCamera_tsm
            // 
            OpenCamera_tsm.Name = "OpenCamera_tsm";
            OpenCamera_tsm.Size = new Size(106, 24);
            OpenCamera_tsm.Text = "连接";
            // 
            // CloseCamera_tsm
            // 
            CloseCamera_tsm.Name = "CloseCamera_tsm";
            CloseCamera_tsm.Size = new Size(106, 24);
            CloseCamera_tsm.Text = "断开";
            // 
            // SnapPictyre_tsm
            // 
            SnapPictyre_tsm.Name = "SnapPictyre_tsm";
            SnapPictyre_tsm.Size = new Size(106, 24);
            SnapPictyre_tsm.Text = "抓图";
            // 
            // omronPLC_tsm
            // 
            omronPLC_tsm.DropDownItems.AddRange(new ToolStripItem[] { plcConnect_tsm, plcDisconnect_tsm });
            omronPLC_tsm.Name = "omronPLC_tsm";
            omronPLC_tsm.Size = new Size(211, 24);
            omronPLC_tsm.Text = "OmronPLC下位机";
            // 
            // plcConnect_tsm
            // 
            plcConnect_tsm.Name = "plcConnect_tsm";
            plcConnect_tsm.Size = new Size(106, 24);
            plcConnect_tsm.Text = "连接";
            // 
            // plcDisconnect_tsm
            // 
            plcDisconnect_tsm.Name = "plcDisconnect_tsm";
            plcDisconnect_tsm.Size = new Size(106, 24);
            plcDisconnect_tsm.Text = "断开";
            // 
            // singlePic_tsm
            // 
            singlePic_tsm.Font = new Font("Microsoft YaHei UI", 10.5F);
            singlePic_tsm.Name = "singlePic_tsm";
            singlePic_tsm.Size = new Size(77, 26);
            singlePic_tsm.Text = "单帧图像";
            singlePic_tsm.Click += SinglePic_tsm_Click;
            // 
            // multiplePic_tsm
            // 
            multiplePic_tsm.Font = new Font("Microsoft YaHei UI", 10.5F);
            multiplePic_tsm.Name = "multiplePic_tsm";
            multiplePic_tsm.Size = new Size(77, 26);
            multiplePic_tsm.Text = "实时图像";
            multiplePic_tsm.Click += MultiplePic_tsm_Click;
            // 
            // singleProcess_tsm
            // 
            singleProcess_tsm.Font = new Font("Microsoft YaHei UI", 10.5F);
            singleProcess_tsm.Name = "singleProcess_tsm";
            singleProcess_tsm.Size = new Size(77, 26);
            singleProcess_tsm.Text = "单次执行";
            singleProcess_tsm.Click += 单次执行ToolStripMenuItem_Click;
            // 
            // multipleProcess_tsm
            // 
            multipleProcess_tsm.Font = new Font("Microsoft YaHei UI", 10.5F);
            multipleProcess_tsm.Name = "multipleProcess_tsm";
            multipleProcess_tsm.Size = new Size(77, 26);
            multipleProcess_tsm.Text = "连续执行";
            // 
            // MainFrm
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(tableLayoutPanel1);
            MainMenuStrip = menuStrip1;
            Margin = new Padding(2);
            Name = "MainFrm";
            Text = "HalconVision";
            groupBox1.ResumeLayout(false);
            flowLayoutPanel1.ResumeLayout(false);
            ImgHandle_panel.ResumeLayout(false);
            TemplateMatch_panel.ResumeLayout(false);
            groupBox3.ResumeLayout(false);
            panel3.ResumeLayout(false);
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            cm_drawJoinLine.ResumeLayout(false);
            processor_grp.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private Panel panel3;
        private Button ImageHandle_btn;
        private Panel ImgHandle_panel;
        private Panel TemplateMatch_panel;
        private Button TemplateMatch_btn;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label loadImage_lbl;
        private Label templateMatch_lab;
        private Label createTemplate_lab;
        private Label drawROI_lab;
        private Label label6;
        private Panel FlowList_panel;
        private GroupBox processor_grp;
        private ContextMenuStrip cm_drawJoinLine;
        private ToolStripMenuItem 连线ToolStripMenuItem;
        private MyHalconControl myhctl;
        private Label calibration_lab;
        private TextBox textBox1;
        private TableLayoutPanel tableLayoutPanel1;
        private FlowLayoutPanel flowLayoutPanel1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem connect_tsm;
        private ToolStripMenuItem halcon_tsm;
        private ToolStripMenuItem mindVision_tsm;
        private ToolStripMenuItem setCamera_tsm;
        private ToolStripMenuItem singlePic_tsm;
        private ToolStripMenuItem multiplePic_tsm;
        private ToolStripMenuItem singleProcess_tsm;
        private ToolStripMenuItem multipleProcess_tsm;
        private ToolStripMenuItem OpenCamera_tsm;
        private ToolStripMenuItem CloseCamera_tsm;
        private ToolStripMenuItem SnapPictyre_tsm;
        private ToolStripMenuItem omronPLC_tsm;
        private ToolStripMenuItem halConnect_tsm;
        private ToolStripMenuItem halDisconnect_tsm;
        private ToolStripMenuItem plcConnect_tsm;
        private ToolStripMenuItem plcDisconnect_tsm;
        private Button processor_btn;
    }
}